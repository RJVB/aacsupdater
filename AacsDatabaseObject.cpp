/*****************************************************************************
 *
 *  AacsDatabaseObject.cpp -- Aacs Database processor (server API 1.0).
 * 
 *  Copyright (C) 2015 labDV - Starbuck <starbuck@labdv.com>
 *
 *  GNU General Public License Usage
 *  This file may be used under the terms of the GNU General Public 
 *  License version 3.0 as published by the Free Software Foundation
 *  and appearing in the file LICENSE.GPL included in the packaging
 *  of this file.  Please review the following information to ensure
 *  the GNU General Public License version 3.0 requirements will be
 *  met: http://www.gnu.org/copyleft/gpl.html.
 *
 *****************************************************************************/

#include "AacsUpdater.h"
#include <shlobj.h>

#define KEYDB_SERVER_API	"2.0"

#ifndef KEYDB_SERVER
#define KEYDB_SERVER "http://localhost"
#endif

#ifndef KEYDB_ACCESS
#define KEYDB_ACCESS "passphrase"
#endif

#include "AacsDatabaseObject.h"

#include <QDesktopWidget>
#include <QApplication>
#include <QDateTime>
#include <QFile>
#include <QFontMetrics.h>
#include <QLayout>
#include <QRegExp>
#include <QRegularExpression>
#include <QRegularExpressionMatch>
#include <QRegularExpressionMatchIterator>
#include <QSettings>
#include <QSet>
#include <QTextStream>
#include <QTimer>

AacsDatabaseObject::AacsDatabaseObject(QObject *parent)
	: QObject (parent)
{
	m_netReply = 0;
	m_netTimer = new QTimer(this);
	m_uploadTimeout = 60000;
}

AacsDatabaseObject::~AacsDatabaseObject()
{
	if (m_netReply) abortNetworkConnection();
}

void AacsDatabaseObject::abortNetworkConnection()
{
	if (!m_netReply) return;

	if (m_netReply->isRunning() || !m_netReply->isFinished()) 
	{
		m_netReply->abort();
		m_netReply->close();
	
		emit messageLog(tr("Check API: abort server connection"));
	}

	m_netReply->deleteLater();
}

void AacsDatabaseObject::handleApiReplyError(QNetworkReply::NetworkError code)
{
	if (m_netReply && m_netReply->error())
		emit messageLog(tr("Check API: network error '%1'").arg(m_netReply->errorString()));
	else
		emit messageLog(tr("Check API: network error #%1").arg(code));
}

void AacsDatabaseObject::handleApiReplyFinished()
{
	m_netTimer->stop();
	m_netTimer->disconnect();

	if (!m_netReply) return;

	int netWorkError = m_netReply->error();

	emit messageLog(tr("Check API: close server connection"));
	m_netReply->deleteLater();
	m_netReply = 0;

	if (netWorkError)
	{
		emit checkApiEnded(false);
		return;
	}

	if (m_responseRaw.isEmpty())
	{
		emit messageLog(tr("Check API: server not responding"));
		emit checkApiEnded(false);
	}
	else
	{
		parseApiReply();

		if (m_updateResponse["UPDATE"] == "none")
		{
			emit messageLog(tr("Check API: program up-to-date"));
			emit checkApiEnded(true);
		}
		else if (m_updateResponse["UPDATE"] == "suggested")
		{
			emit messageLog(tr("Check API: upgrade available"));
			emit checkApiEnded(true);
		}
		else if (m_updateResponse["UPDATE"] == "required")
		{
			emit messageLog(tr("Check API: upgrade required"));
			emit checkApiEnded(true);
		}
		else
		{
			emit messageLog(tr("Check API: unexpected server response"));
			emit checkApiEnded(false);
		}
	}
}

void AacsDatabaseObject::handleApiReplyRead()
{
	if (m_netReply)
	{
		QByteArray buf = m_netReply->readAll();

		m_responseRaw.append(buf);
		emit dataDump(tr("API reply"), QString(buf).toHtmlEscaped());	
	}
}

void AacsDatabaseObject::handleApiReplyTimedOut()
{
	m_netTimer->stop();
	m_netTimer->disconnect();

	abortNetworkConnection();

	emit checkApiTimedOut();
}

void AacsDatabaseObject::handleCentralReplyError(QNetworkReply::NetworkError code)
{
	if (m_netReply && m_netReply->error())
		emit messageLog(tr("Central KEYDB.cfg: network error '%1'").arg(m_netReply->errorString()));
	else
		emit messageLog(tr("Central KEYDB.cfg: network error #%1").arg(code));
}

void AacsDatabaseObject::handleCentralReplyFinished()
{
	m_netTimer->stop();
	m_netTimer->disconnect();

	m_centralDisc.clear();
	m_centralDiscName.clear();
	m_centralDuplicateDiscName.clear();
	m_centralHc.clear();
	m_centralPk.clear();
	m_centralVuk.clear();
	
	if (!m_netReply) return;

	int netWorkError = m_netReply->error();

	emit messageLog(tr("Central KEYDB.cfg: close server connection"));
	m_netReply->deleteLater();
	m_netReply = 0;

	if (netWorkError)
	{
		emit loadCentralKeydbEnded(false);
		return;
	}

	if (m_responseRaw.isEmpty())
	{
		emit messageLog(tr("Central KEYDB.cfg: server not responding"));
		emit loadCentralKeydbEnded(false);
	}
	else
	{
		m_updateResponse["KEYDB"] = m_responseRaw;

		if (m_updateResponse["KEYDB"].startsWith("ERROR"))
		{
			emit messageLog(tr("Central KEYDB.cfg: error accessing central database"));
			emit loadCentralKeydbEnded(false);
		}
		else
		{
			emit messageLog(tr("Central KEYDB.cfg: AACS keys downloaded from central database"));
			parseCentralReply();
			emit loadCentralKeydbEnded(true);
		}
	}
}

void AacsDatabaseObject::handleCentralReplyRead()
{
	if (m_netReply)
	{
		QByteArray buf = m_netReply->readAll();

		m_responseRaw.append(buf);
		emit dataDump(tr("Central DB"), QString::fromUtf8(buf));
	}

	qApp->processEvents();
}

void AacsDatabaseObject::handleCentralReplyTimedOut()
{
	m_netTimer->stop();
	m_netTimer->disconnect();

	abortNetworkConnection();

	emit loadCentralKeydbTimedOut();
}

void AacsDatabaseObject::handleUploadReplyError(QNetworkReply::NetworkError code)
{
	if (m_netReply && m_netReply->error())
		emit messageLog(tr("Upload local keys: network error '%1'").arg(m_netReply->errorString()));
	else
		emit messageLog(tr("Upload local keys: network error #%1").arg(code));
}

void AacsDatabaseObject::handleUploadReplyFinished()
{
	m_netTimer->stop();
	m_netTimer->disconnect();

	if (!m_netReply) return;

	int netWorkError = m_netReply->error();

	emit messageLog(tr("Upload local keys: close server connection"));
	m_netReply->deleteLater();
	m_netReply = 0;

	if (netWorkError)
	{
		emit uploadNewKeysEnded(m_newLocalKeysFound, false);
		return;
	}

	if (m_responseRaw.isEmpty())
	{
		emit messageLog(tr("Upload local keys: server not responding"));
		emit uploadNewKeysEnded(m_newLocalKeysFound, false);
	}
	else
	{
		QRegExp rx("<UPLOAD>(.*)</UPLOAD>");
		rx.indexIn(m_responseRaw);
		if (rx.captureCount() == 1) m_updateResponse["UPLOAD"] = rx.cap(1).trimmed().toUtf8();

		if (m_updateResponse["UPLOAD"] != "done")
		{
			emit messageLog(tr("Upload local keys: error accessing central database"));
			emit uploadNewKeysEnded(m_newLocalKeysFound, false);
		}
		else
		{
			emit messageLog(tr("Upload local keys: AACS keys uploaded to central database"));
			emit uploadNewKeysEnded(m_newLocalKeysFound, true);
		}
	}
}

void AacsDatabaseObject::handleUploadReplyRead()
{
	if (m_netReply)
	{
		QByteArray buf = m_netReply->readAll();

		m_responseRaw.append(buf);
		emit dataDump(tr("Upload reply"), QString(buf).toHtmlEscaped());	
	}

	qApp->processEvents();
}

void AacsDatabaseObject::handleUploadReplyTimedOut()
{
	m_netTimer->stop();
	m_netTimer->disconnect();

	abortNetworkConnection();

	emit uploadNewKeysTimedOut();
}

QString AacsDatabaseObject::newDiscKeys() const
{
	return m_newDiscKeys;
}

QString AacsDatabaseObject::newLocalKeys() const
{
	return m_newLocalKeys;
}

void AacsDatabaseObject::parseApiReply()
{
	// XML answer:  <UPDATE> none/suggested/required
	//				<VERSION> available version
	//				<FILEBIN> download file name	
	//				<URLBIN> download link	
	//				<FILESRC> source download file name	
	//				<URLSRC> source download link	
	QRegExp rx;
	rx.setCaseSensitivity(Qt::CaseSensitive);
	rx.setMinimal(false);

	rx.setPattern("<UPDATE>(.*)</UPDATE>");
	rx.indexIn(m_responseRaw);
	if (rx.captureCount() == 1) m_updateResponse["UPDATE"] = rx.cap(1).trimmed().toUtf8();

	rx.setPattern("<FILEBIN>(.*)</FILEBIN>");
	rx.indexIn(m_responseRaw);
	if (rx.captureCount() == 1) m_updateResponse["FILEBIN"] = rx.cap(1).trimmed().toUtf8();

	rx.setPattern("<URLBIN>(.*)</URLBIN>");
	rx.indexIn(m_responseRaw);
	if (rx.captureCount() == 1) m_updateResponse["URLBIN"] = rx.cap(1).trimmed().toUtf8();

	rx.setPattern("<FILESRC>(.*)</FILESRC>");
	rx.indexIn(m_responseRaw);
	if (rx.captureCount() == 1) m_updateResponse["FILESRC"] = rx.cap(1).trimmed().toUtf8();

	rx.setPattern("<URLSRC>(.*)</URLSRC>");
	rx.indexIn(m_responseRaw);
	if (rx.captureCount() == 1) m_updateResponse["URLSRC"] = rx.cap(1).trimmed().toUtf8();

	rx.setPattern("<VERSION>(.*)</VERSION>");
	rx.indexIn(m_responseRaw);
	if (rx.captureCount() == 1) m_updateResponse["VERSION"] = rx.cap(1).trimmed().toUtf8();
}

void AacsDatabaseObject::parseCentralReply()
{
	emit updateProcessingMessage(tr("Analyzing AACS keys from central database..."));

	if (!m_updateResponse["KEYDB"].isEmpty())
	{
		QString keys(m_updateResponse["KEYDB"]);
		keys.replace("\\\n", " ");

		// PK in central KEYDB.cfg
		QRegExp rx(QString("\\|\\s+PK\\s+\\|\\s+0x([0-9ABCDEFabcdef]{32,32})(?:[^\\n^;^\\|]*;([^\\n^;^\\|]+))*"));
		rx.setCaseSensitivity(Qt::CaseSensitive);
		rx.setMinimal(false);
		int pos(0);

		while ((pos = rx.indexIn(keys, pos)) != -1)
		{
			pos += rx.matchedLength();
			QString pk = rx.cap(1).toUpper();
			QString comment = rx.cap(2).trimmed();

			m_centralPk[pk] = comment;

			if (!comment.isEmpty())
				m_centralPk[pk] =  QString("| PK | 0x%1 ; %2").arg(pk).arg(comment);
			else
				m_centralPk[pk] = QString("| PK | 0x%1").arg(pk);

			qApp->processEvents();  // GUI responsiveness
		}
	
		// HC in central KEYDB.cfg
		rx.setPattern(QString(
"(?:;([^\\n]+)\\n)*"
"\\|\\s+HC\\s+\\|\\s+HOST_PRIV_KEY\\s+0x([0-9ABCDEFabcdef]{40,40})\\s+"
"\\|\\s+HOST_CERT\\s+0x([0-9ABCDEFabcdef]{40,40})\\s+"
"0x([0-9ABCDEFabcdef]{40,40})\\s+"
"0x([0-9ABCDEFabcdef]{40,40})\\s+"
"0x([0-9ABCDEFabcdef]{40,40})\\s+"
"0x([0-9ABCDEFabcdef]{24,24})\\s+"
"\\|\\s+HOST_NONCE\\s+0x([0-9ABCDEFabcdef]{40,40})\\s+"
"\\|\\s+HOST_KEY_POINT\\s+0x([0-9ABCDEFabcdef]{40,40})\\s+"
"0x([0-9ABCDEFabcdef]{40,40})"));
			
		rx.setCaseSensitivity(Qt::CaseSensitive);
		rx.setMinimal(false);
		pos = 0;

		while ((pos = rx.indexIn(keys, pos)) != -1)
		{
			pos += rx.matchedLength();

			QString comment = rx.cap(1).trimmed();
			QString hc;
			for (int i = 2; i <= 10; i++) hc += rx.cap(i).toUpper();

			if (comment.isEmpty())
				m_centralHc[hc] = QString(
"| HC | HOST_PRIV_KEY 0x%1 \\\n"
"     | HOST_CERT 0x%2 \\\n"
"                 0x%3 \\\n"
"                 0x%4 \\\n"
"                 0x%5 \\\n"
"                 0x%6 \\\n"
"     | HOST_NONCE 0x%7 \\\n"
"     | HOST_KEY_POINT 0x%8 \\\n"
"                      0x%9").arg(rx.cap(2).toUpper()).arg(rx.cap(3).toUpper()).arg(rx.cap(4).toUpper()).arg(rx.cap(5).toUpper())
							.arg(rx.cap(6).toUpper()).arg(rx.cap(7).toUpper()).arg(rx.cap(8).toUpper()).arg(rx.cap(9).toUpper())
							.arg(rx.cap(10).toUpper());			
			else
				m_centralHc[hc] = QString(
"; %1\n"
"| HC | HOST_PRIV_KEY 0x%2 \\\n"
"     | HOST_CERT 0x%3 \\\n"
"                 0x%4 \\\n"
"                 0x%5 \\\n"
"                 0x%6 \\\n"
"                 0x%7 \\\n"
"     | HOST_NONCE 0x%8 \\\n"
"     | HOST_KEY_POINT 0x%9 \\\n"
"                      0x%10").arg(comment)
							.arg(rx.cap(2).toUpper()).arg(rx.cap(3).toUpper()).arg(rx.cap(4).toUpper()).arg(rx.cap(5).toUpper())
							.arg(rx.cap(6).toUpper()).arg(rx.cap(7).toUpper()).arg(rx.cap(8).toUpper()).arg(rx.cap(9).toUpper())
							.arg(rx.cap(10).toUpper());			

			qApp->processEvents();  // GUI responsiveness
		}
	
		// VUK in central KEYDB.cfg
		//rx.setPattern(QString("0x([0-9ABCDEFabcdef]{40,40})\\s*=([^\\|]*)\\|\\s+V\\s+\\|\\s+0x([0-9ABCDEFabcdef]{32,32})(?:[^\\n^;^\\|]*;([^\\n^;^\\|]+))*"));
		
		// nalor (doom9) regex: ^\s*(?:0x)?(?<discid>[0-9ABCDEFabcdef]{40,40})\s*=\s*(?<title>[^\|]*)(?:\|\s+(?:(?:(?:[V])\s+\|\s+(?:0x)?(?<vuk>[0-9ABCDEFabcdef]{32,32})[^\|;]*)|(?:(?:[D])\s+\|\s+(?<date>\d{4}-\d{2}-\d{2})[^\|;]*)|(?:(?:[^DV])\s+\|\s+[^\|;]*)))+(?:;\s*(?<comment>.*))?		
		QRegularExpression re(QString("^\\s*(?:0x)?(?<discid>[0-9ABCDEFabcdef]{40,40})\\s*=\\s*(?<title>[^\\|]*)(?:\\|\\s+(?:(?:(?:[V])\\s+\\|\\s+(?:0x)?(?<vuk>[0-9ABCDEFabcdef]{32,32})[^\\|;]*)|(?:(?:[D])\\s+\\|\\s+(?<date>\\d{4}[/\\-]\\d{2}[/\\-]\\d{2})[^\\|;]*)|(?:(?:[^DV])\\s+\\|\\s+[^\\|;]*)))+(?:;\\s*(?<comment>.*))?"));

		re.setPatternOptions(QRegularExpression::OptimizeOnFirstUsageOption);
		
		if (!re.isValid())
			qDebug() << "INVALID";

		QTextStream stream(&keys, QIODevice::ReadOnly);
		QString line;
		while (stream.readLineInto(&line))
		{
			QRegularExpressionMatch match = re.match(line);

			if (match.hasMatch())
			{
				QString disc_id = match.captured("discid").toUpper();
				QString disc_name = match.captured("title").trimmed().replace("\"", "").replace("|", "");
				QString vuk = match.captured("vuk").toUpper();
				QString comment = match.captured("comment").trimmed();

				if (disc_name.isEmpty())
					disc_name = "_NONAME_";
				else
					m_centralDiscName.insertMulti(disc_id, disc_name);

				if (comment.isEmpty())
					m_centralVuk[disc_id + vuk] = QString("0x%1 = %2 | V | 0x%3")
					.arg(disc_id).arg(disc_name).arg(vuk);
				else
					m_centralVuk[disc_id + vuk] = QString("0x%1 = %2 | V | 0x%3 ; %4")
					.arg(disc_id).arg(disc_name).arg(vuk).arg(comment);

				m_centralDisc[disc_id] = 1 + m_centralDisc.value(disc_id, 0);
			}

			qApp->processEvents();  // GUI responsiveness
		}
	}

	if (!m_centralPk.isEmpty())
	{
		emit dataDump(tr("Parsing"), tr("Central PK:"));
		QMapIterator<QString,QString> pk(m_centralPk);
		while (pk.hasNext())
		{
			pk.next();
			emit dataDump(tr("Parsing"), pk.value());
		}
	}

	if (!m_centralHc.isEmpty())
	{
		emit dataDump(tr("Parsing"), tr("Central HC:"));
		QMapIterator<QString,QString> hc(m_centralHc);
		while (hc.hasNext())
		{
			hc.next();
			emit dataDump(tr("Parsing"), hc.value());
		}
	}

	if (!m_centralVuk.isEmpty())
	{
		emit dataDump(tr("Parsing"), tr("Central VUK:"));
		QListIterator<QString> vuk(m_centralVuk.values());
		while (vuk.hasNext())
		{
			emit dataDump(tr("Parsing"), vuk.next());

			qApp->processEvents();  // GUI responsiveness
		}
	}

	QMapIterator<QString,int> d(m_centralDisc);
	while (d.hasNext())
	{
		qApp->processEvents();  // GUI responsiveness

		d.next();
		if (d.value() < 2) continue;
		m_centralDuplicateDiscName[d.key()] = m_centralDiscName.values(d.key());
	}

	if (!m_centralDuplicateDiscName.isEmpty())
	{
		emit dataDump(tr("Disc names"), tr("Central disc names duplicate:"));
		QMapIterator<QString,QStringList> d(m_centralDuplicateDiscName);
		while (d.hasNext())
		{
			d.next();
			QStringListIterator n(d.value());
			while (n.hasNext())
			{
				emit dataDump(tr("Disc names"), QString("%1 = %2").arg(d.key()).arg(n.next()));

				qApp->processEvents();  // GUI responsiveness
			}
		}

		emit dataDump(tr("Disc names"), "\n");
	}

	emit messageLog(tr("Central KEYDB.cfg: %1 PK").arg(m_centralPk.count()));
	emit messageLog(tr("Central KEYDB.cfg: %1 HC").arg(m_centralHc.count()));
	emit messageLog(tr("Central KEYDB.cfg: %1 VUK for %2 discs").arg(m_centralVuk.count()).arg(m_centralDisc.count()));
}

void AacsDatabaseObject::parseLocalKeydb()
{
	emit updateProcessingMessage(tr("Analyzing AACS keys from local KEYDB.CFG..."));

	QString appDataVUKs;

	if (!m_localKeydb.isEmpty())
	{
		QString keys(m_localKeydb);
		keys.replace("\\\n", " ");

		// PK in local KEYDB.cfg
		QRegExp rx(QString("\\|\\s+PK\\s+\\|\\s+0x([0-9ABCDEFabcdef]{32,32})(?:[^\\n^;^\\|]*;([^\\n^;^\\|]+))*"));
		rx.setCaseSensitivity(Qt::CaseSensitive);
		rx.setMinimal(false);
		int pos(0);

		while ((pos = rx.indexIn(keys, pos)) != -1)
		{
			qApp->processEvents();  // GUI responsiveness

			pos += rx.matchedLength();

			QString pk = rx.cap(1).toUpper();
			QString comment = rx.cap(2).trimmed();

			if (!comment.isEmpty())
				m_localPk[pk] = QString("| PK | 0x%1 ; %2").arg(pk).arg(comment);
			else
				m_localPk[pk] = QString("| PK | 0x%1").arg(pk);
		}
	
		qApp->processEvents();  // GUI responsiveness

		// HC in local KEYDB.cfg
		rx.setPattern(QString(
"(?:;([^\\n]+)\\n)*"
"\\|\\s+HC\\s+\\|\\s+HOST_PRIV_KEY\\s+0x([0-9ABCDEFabcdef]{40,40})\\s+"
"\\|\\s+HOST_CERT\\s+0x([0-9ABCDEFabcdef]{40,40})\\s+"
"0x([0-9ABCDEFabcdef]{40,40})\\s+"
"0x([0-9ABCDEFabcdef]{40,40})\\s+"
"0x([0-9ABCDEFabcdef]{40,40})\\s+"
"0x([0-9ABCDEFabcdef]{24,24})\\s+"
"\\|\\s+HOST_NONCE\\s+0x([0-9ABCDEFabcdef]{40,40})\\s+"
"\\|\\s+HOST_KEY_POINT\\s+0x([0-9ABCDEFabcdef]{40,40})\\s+"
"0x([0-9ABCDEFabcdef]{40,40})"));
			
		rx.setCaseSensitivity(Qt::CaseSensitive);
		rx.setMinimal(false);
		pos = 0;

		while ((pos = rx.indexIn(keys, pos)) != -1)
		{
			qApp->processEvents();  // GUI responsiveness

			pos += rx.matchedLength();

			QString comment = rx.cap(1).trimmed();
			QString hc;
			for (int i = 2; i <= 10; i++) hc += rx.cap(i).toUpper();

			if (comment.isEmpty())
				m_localHc[hc] = QString(
"| HC | HOST_PRIV_KEY 0x%1 \\\n"
"     | HOST_CERT 0x%2 \\\n"
"                 0x%3 \\\n"
"                 0x%4 \\\n"
"                 0x%5 \\\n"
"                 0x%6 \\\n"
"     | HOST_NONCE 0x%7 \\\n"
"     | HOST_KEY_POINT 0x%8 \\\n"
"                      0x%9").arg(rx.cap(2).toUpper()).arg(rx.cap(3).toUpper()).arg(rx.cap(4).toUpper()).arg(rx.cap(5).toUpper())
							.arg(rx.cap(6).toUpper()).arg(rx.cap(7).toUpper()).arg(rx.cap(8).toUpper()).arg(rx.cap(9).toUpper())
							.arg(rx.cap(10).toUpper());			
			else
				m_localHc[hc] = QString(
"; %1\n"
"| HC | HOST_PRIV_KEY 0x%2 \\\n"
"     | HOST_CERT 0x%3 \\\n"
"                 0x%4 \\\n"
"                 0x%5 \\\n"
"                 0x%6 \\\n"
"                 0x%7 \\\n"
"     | HOST_NONCE 0x%8 \\\n"
"     | HOST_KEY_POINT 0x%9 \\\n"
"                      0x%10").arg(comment)
							.arg(rx.cap(2).toUpper()).arg(rx.cap(3).toUpper()).arg(rx.cap(4).toUpper()).arg(rx.cap(5).toUpper())
							.arg(rx.cap(6).toUpper()).arg(rx.cap(7).toUpper()).arg(rx.cap(8).toUpper()).arg(rx.cap(9).toUpper())
							.arg(rx.cap(10).toUpper());			

			qApp->processEvents();  // GUI responsiveness
		}
	
		qApp->processEvents();  // GUI responsiveness

		// VUK in local KEYDB.cfg
		//rx.setPattern(QString("(;*)[^\\n]*0x([0-9ABCDEFabcdef]{40,40})\\s*=([^\\|]*)\\|\\s+V\\s+\\|\\s+0x([0-9ABCDEFabcdef]{32,32})(?:[^\\n^;^\\|]*;([^\\n^;^\\|]+))*"));

		// nalor (doom9) regex: ^\s*(?:0x)?(?<discid>[0-9ABCDEFabcdef]{40,40})\s*=\s*(?<title>[^\|]*)(?:\|\s+(?:(?:(?:[V])\s+\|\s+(?:0x)?(?<vuk>[0-9ABCDEFabcdef]{32,32})[^\|;]*)|(?:(?:[D])\s+\|\s+(?<date>\d{4}-\d{2}-\d{2})[^\|;]*)|(?:(?:[^DV])\s+\|\s+[^\|;]*)))+(?:;\s*(?<comment>.*))?		
		QRegularExpression re(QString("^\\s*(?:0x)?(?<discid>[0-9ABCDEFabcdef]{40,40})\\s*=\\s*(?<title>[^\\|]*)(?:\\|\\s+(?:(?:(?:[V])\\s+\\|\\s+(?:0x)?(?<vuk>[0-9ABCDEFabcdef]{32,32})[^\\|;]*)|(?:(?:[D])\\s+\\|\\s+(?<date>\\d{4}[/\\-]\\d{2}[/\\-]\\d{2})[^\\|;]*)|(?:(?:[^DV])\\s+\\|\\s+[^\\|;]*)))+(?:;\\s*(?<comment>.*))?"));

		re.setPatternOptions(QRegularExpression::OptimizeOnFirstUsageOption);

		if (!re.isValid())
			qDebug() << "INVALID";

		QTextStream stream(&keys, QIODevice::ReadOnly);
		QString line;
		while (stream.readLineInto(&line))
		{
			QRegularExpressionMatch match = re.match(line);

			if (match.hasMatch())
			{
				QString disc_id = match.captured("discid").toUpper();
				QString disc_name = match.captured("title").trimmed().replace("\"", "").replace("|", "");
				QString vuk = match.captured("vuk").toUpper();
				QString comment = match.captured("comment").trimmed();

				if (disc_name.isEmpty())
					disc_name = "_NONAME_";
				else
					m_localDiscName.insertMulti(disc_id, disc_name);

				if (comment.isEmpty())
					m_localVuk[disc_id + vuk] = QString("0x%1 = %2 | V | 0x%3")
					.arg(disc_id).arg(disc_name).arg(vuk);
				else
					m_localVuk[disc_id + vuk] = QString("0x%1 = %2 | V | 0x%3 ; %4")
					.arg(disc_id).arg(disc_name).arg(vuk).arg(comment);

				m_localDisc[disc_id] = 1 + m_localDisc.value(disc_id, 0);
			}

			qApp->processEvents();  // GUI responsiveness
		}
	}
	
	if (!m_localPk.isEmpty())
	{
		emit dataDump(tr("Parsing"), tr("Local PK:"));
		QMapIterator<QString,QString> pk(m_localPk);
		while (pk.hasNext())
		{
			pk.next();
			emit dataDump(tr("Parsing"), pk.value());

			qApp->processEvents();  // GUI responsiveness
		}
	}

	if (!m_localHc.isEmpty())
	{
		emit dataDump(tr("Parsing"), tr("Local HC:"));
		QMapIterator<QString,QString> hc(m_localHc);
		while (hc.hasNext())
		{
			hc.next();
			emit dataDump(tr("Parsing"), hc.value());

			qApp->processEvents();  // GUI responsiveness
		}
	}

	if (!m_localVuk.isEmpty())
	{
		emit dataDump(tr("Parsing"), tr("Local VUK:"));
		QListIterator<QString> vuk(m_localVuk.values());
		while (vuk.hasNext())
		{
			emit dataDump(tr("Parsing"), vuk.next());

			qApp->processEvents();  // GUI responsiveness
		}
	}
}

void AacsDatabaseObject::startCheckApi(int timeout)
{
	if (m_netReply && m_netReply->isRunning()) return;  // once at a time

	m_updateResponse.clear();

	QNetworkAccessManager *accessManager = new QNetworkAccessManager(this);
	QNetworkConfigurationManager configurationManager;
	accessManager->setConfiguration(configurationManager.defaultConfiguration());
	if (accessManager->networkAccessible() != QNetworkAccessManager::Accessible)
	{
		// sounds to tough, we try to connect and we'll get a further error if not accessible
		//return;
	}

	// Show data in log (like passprhrase)
	bool debug(false);
#ifdef _DEBUG 
	debug = true;
#endif

	QString dataToEncode =  QString("a=%1&s=%2&p=%3&v=%4&d=%5")
		.arg("update").arg(KEYDB_SERVER_API).arg(KEYDB_ACCESS).arg(APPVERSION_STR).arg(debug);

	QByteArray data = QUrl::toPercentEncoding(dataToEncode, "&=");  // space aren't + so might be %20 so NOT LIKE HTML FORMS

	QNetworkRequest request;
	request.setUrl(QUrl(KEYDB_SERVER));
	request.setRawHeader("Referer", QString("Updater %1").arg(APPVERSION_STR).toLatin1());
	request.setRawHeader("User-Agent", QString("AacsDatabaseObject").toLatin1());
	request.setRawHeader("Content-Type", QString("application/x-www-form-urlencoded; charset=UTF-8").toLatin1());

	emit messageLog(tr("Check API: connecting to server"));
	
	connect(m_netTimer, SIGNAL(timeout()), SLOT(handleApiReplyTimedOut()));
	m_netTimer->start(timeout);

	m_responseRaw.clear();	
	m_netReply = accessManager->post(request, data);

	connect(m_netReply, SIGNAL(error(QNetworkReply::NetworkError)), SLOT(handleApiReplyError(QNetworkReply::NetworkError)));
	connect(m_netReply, SIGNAL(finished()), SLOT(handleApiReplyFinished()));
	connect(m_netReply, SIGNAL(readyRead()), SLOT(handleApiReplyRead()));
}

void AacsDatabaseObject::startLoadCentralKeydb(int timeout)
{
	if (m_netReply && m_netReply->isRunning()) return;  // once at a time

	m_uploadTimeout = timeout * 10;  // upload Internet connection are asymetric

	m_updateResponse.clear();

	QNetworkAccessManager *accessManager = new QNetworkAccessManager(this);
	QNetworkConfigurationManager configurationManager;
	accessManager->setConfiguration(configurationManager.defaultConfiguration());
	if (accessManager->networkAccessible() != QNetworkAccessManager::Accessible)
	{
		// sounds to tough, we try to connect and we'll get a further error if not accessible
		//return;
	}

	QString dataToEncode =  QString("a=%1&s=%2&p=%3&v=%4&d=%5")
		.arg("keydb").arg(KEYDB_SERVER_API).arg(KEYDB_ACCESS).arg(APPVERSION_STR).arg(0);

	QByteArray data = QUrl::toPercentEncoding(dataToEncode, "&=");  // space aren't + so might be %20 so NOT LIKE HTML FORMS

	QNetworkRequest request;
	request.setUrl(QUrl(KEYDB_SERVER));
	request.setRawHeader("Referer", QString("Updater %1").arg(APPVERSION_STR).toLatin1());
	request.setRawHeader("User-Agent", QString("AacsDatabaseObject").toLatin1());
	request.setRawHeader("Content-Type", QString("application/x-www-form-urlencoded; charset=UTF-8").toLatin1());

	emit messageLog(tr("Central KEYDB.cfg: connecting to server"));
	
	connect(m_netTimer, SIGNAL(timeout()), SLOT(handleCentralReplyTimedOut()));
	m_netTimer->start(timeout);

	m_responseRaw.clear();	
	m_netReply = accessManager->post(request, data);

	connect(m_netReply, SIGNAL(error(QNetworkReply::NetworkError)), SLOT(handleCentralReplyError(QNetworkReply::NetworkError)));
	connect(m_netReply, SIGNAL(finished()), SLOT(handleCentralReplyFinished()));
	connect(m_netReply, SIGNAL(readyRead()), SLOT(handleCentralReplyRead()));
}

void AacsDatabaseObject::startParseLocalConfig()
{
	m_appDataVuk.clear();
	m_localDisc.clear();
	m_localDiscName.clear();
	m_localDuplicateDiscName.clear();
	m_localHc.clear();
	m_localPk.clear();
	m_localVuk.clear();

#if defined(WIN32)
	//QString appData = QString::fromUtf8(qgetenv("APPDATA"));
	QString appData;
	LPWSTR wszPath = NULL;
	HRESULT hr = SHGetKnownFolderPath(FOLDERID_RoamingAppData, KF_FLAG_CREATE, NULL, &wszPath);
	if (SUCCEEDED(hr))
	{
		appData = QString::fromWCharArray(wszPath);
	}
	else
	{
		appData = QString::fromUtf8(qgetenv("APPDATA"));
	}
	
	//if (wszPath) delete[] wszPath;

	QString localKeydb = appData + "\\aacs\\KEYDB.cfg";
	QFile localFile(localKeydb);
	
	if (localFile.exists())
	{
		emit updateProcessingMessage(tr("Loading AACS keys from local KEYDB.CFG..."));

		if (localFile.open(QIODevice::ReadOnly | QIODevice::Text))
		{
			QTextStream in(&localFile);
			QTextStream out(&m_localKeydb);
			in.setCodec("UTF-8");

			QString line;
			while (in.readLineInto(&line))
			{
				out << line << "\n";

				emit dataDump(tr("KEYDB.cfg"), line);

				qApp->processEvents();  // GUI responsiveness
			}

			localFile.close();

			emit messageLog(tr("Local KEYDB.cfg: read file '%1'").arg(localKeydb));
		}
		else
		{
			emit messageLog(tr("Local KEYDB.cfg: failed to open file '%1'").arg(localKeydb));
		}
	}
	else
	{
		emit messageLog(tr("Local KEYDB.cfg: file not found  '%1'").arg(localKeydb));
	}
#endif

	parseLocalKeydb();

#if defined(WIN32)
	QString appDataVuk = appData + "\\aacs\\vuk";
	QDir localVukDir(appDataVuk);
	QMap<QString, QString> appDataFile;

	if (localVukDir.exists())
	{
		QStringList list = localVukDir.entryList( QDir::Files | QDir::NoDotAndDotDot );

		if (list.isEmpty())
		{
			emit messageLog(tr("Local %APPDATA%\\aacs\\vuk: no VUK found in '%1'").arg(appDataVuk));
		}
		else
		{
			QStringListIterator vuk(list);
			while (vuk.hasNext())
			{
				qApp->processEvents();  // GUI responsiveness

				QString path = vuk.next();

				if (QRegExp("^[0-9ABCDEFabcdef]{40,40}$").exactMatch(path))
				{
					QFile f(appDataVuk + "\\" + path);
	
					if (f.open(QIODevice::ReadOnly | QIODevice::Text))
					{
						QString vuk = f.readAll();
						f.close();

						if (QRegExp("^[0-9ABCDEFabcdef]{32,32}$").exactMatch(vuk))
						{
							appDataFile[path.toUpper()] = vuk.toUpper();

							emit messageLog(tr("Local %APPDATA%\\aacs\\vuk: %1 -> %2").arg(path.toUpper()).arg(vuk.toUpper()));
						}
					}
					else
					{
						emit messageLog(tr("Local %APPDATA%\\aacs\\vuk: failed to open VUK file '%1'").arg(appDataVuk + "\\" + path));
					}
				}
			}
		}
	}
	else
	{
		emit messageLog(tr("Local %APPDATA%\\aacs\\vuk: VUK folder not found '%1'").arg(appDataVuk));
	}

	qApp->processEvents();
#endif

	if (!appDataFile.isEmpty())
	{
		// VUK from libaacs to add in local KEYDB.cfg
		// 0xDC3934F459F30018D6A067CE4BFC8CBB81B4A321 = _NONAME_ | V | 0xBC916F1AED8F6378F175EB719380635E ; libaacs
		QMapIterator<QString, QString> k(appDataFile);
		while (k.hasNext())
		{
			qApp->processEvents();  // GUI responsiveness

			k.next();
			QRegExp rx(QString("0x%1\\s+=.+\\|\\s+V\\s+\\|\\s+0x%2").arg(k.key()).arg(k.value()));
			rx.setCaseSensitivity(Qt::CaseSensitive);
			rx.setMinimal(false);

			if (rx.indexIn(m_updateResponse["KEYDB"]) == -1)
			{
				m_appDataVuk[k.key() + k.value()] = QString("0x%1 = _NONAME_ | V | 0x%2 ; libaacs").arg(k.key()).arg(k.value());
			}
		}
	}
	
	if (!m_appDataVuk.isEmpty())
	{
		emit dataDump(tr("Parsing"), tr("Local VUK in %APPDATA%\\aacs\\vuk:"));
		QListIterator<QString> vuk(m_appDataVuk.values());
		while (vuk.hasNext())
		{
			emit dataDump(tr("Parsing"), vuk.next());

			qApp->processEvents();  // GUI responsiveness
		}
	}

	QMapIterator<QString,int> d(m_localDisc);
	while (d.hasNext())
	{
		qApp->processEvents();  // GUI responsiveness

		d.next();
		if (d.value() < 2) continue;
		m_localDuplicateDiscName[d.key()] = m_localDiscName.values(d.key());
	}

	if (!m_localDuplicateDiscName.isEmpty())
	{
		emit dataDump(tr("Disc names"), tr("Local disc names duplicate:"));
		QMapIterator<QString,QStringList> d(m_localDuplicateDiscName);
		while (d.hasNext())
		{
			d.next();
			QStringListIterator n(d.value());
			while (n.hasNext())
			{
				emit dataDump(tr("Disc names"), QString("%1 = %2").arg(d.key()).arg(n.next()));

				qApp->processEvents();  // GUI responsiveness
			}
		}
	}

	emit messageLog(tr("Local keys in %APPDATA%: %1 VUK of which %2 new").arg(appDataFile.count()).arg(m_appDataVuk.count()));
	emit messageLog(tr("Local AACS keys: %1 PK").arg(m_localPk.count()));
	emit messageLog(tr("Local AACS keys: %1 HC").arg(m_localHc.count()));
	emit messageLog(tr("Local AACS keys: %1 VUK for %2 discs").arg(m_localVuk.count()).arg(m_localDisc.count()));

	startFindNewKeys();
}

void AacsDatabaseObject::startFindNewKeys()
{
	emit updateProcessingMessage(tr("Extracting new AACS keys from central database..."));

	m_newCentralKeysFound = 0;
	m_newLocalKeysFound = 0;

	QSet<QString> newCentralPk = QSet<QString>::fromList(m_centralPk.keys()) - QSet<QString>::fromList(m_localPk.keys());
	qApp->processEvents();  // GUI responsiveness
	QSet<QString> newLocalPk = QSet<QString>::fromList(m_localPk.keys()) - QSet<QString>::fromList(m_centralPk.keys());
	qApp->processEvents();  // GUI responsiveness

	QSet<QString> newCentralHc = QSet<QString>::fromList(m_centralHc.keys()) - QSet<QString>::fromList(m_localHc.keys());
	qApp->processEvents();  // GUI responsiveness
	QSet<QString> newLocalHc = QSet<QString>::fromList(m_localHc.keys()) - QSet<QString>::fromList(m_centralHc.keys());
	qApp->processEvents();  // GUI responsiveness

	QSet<QString> newCentralVuk = QSet<QString>::fromList(m_centralVuk.keys()) - QSet<QString>::fromList(m_localVuk.keys());
	qApp->processEvents();  // GUI responsiveness
	QSet<QString> newLocalVuk = QSet<QString>::fromList(m_localVuk.keys()) - QSet<QString>::fromList(m_centralVuk.keys());	
	qApp->processEvents();  // GUI responsiveness

	m_newCentralKeysFound = newCentralPk.count() + newCentralHc.count() + newCentralVuk.count();
	qApp->processEvents();  // GUI responsiveness
	m_newLocalKeysFound = newLocalPk.count() + newLocalHc.count() + newLocalVuk.count();
	qApp->processEvents();  // GUI responsiveness

	m_newCentralKeys.clear();
	QTextStream down(&m_newCentralKeys);

	if (!newCentralPk.isEmpty())
	{
		down << "\n";
	
		QSetIterator<QString> cpk(newCentralPk);
		while (cpk.hasNext())
		{
			qApp->processEvents();  // GUI responsiveness

			down << m_centralPk[cpk.next()] << "\n";
		}
	}

	if (!newCentralHc.isEmpty())
	{
		down << "\n";

		QSetIterator<QString> chc(newCentralHc);
		while (chc.hasNext())
		{
			qApp->processEvents();  // GUI responsiveness

			down << m_centralHc[chc.next()] << "\n";
		}
	}

	qApp->processEvents();  // GUI responsiveness

	m_newDiscKeys.clear();

	if (!newCentralVuk.isEmpty())
	{
		down << "\n";

		QSetIterator<QString> cvuk(newCentralVuk);
		while (cvuk.hasNext())
		{
			QString vuk = cvuk.next();
			
			down << m_centralVuk[vuk] << "\n";

			m_newDiscKeys.append(m_centralVuk[vuk] + "\n");

			qApp->processEvents();  // GUI responsiveness
		}
	}

	if (m_newCentralKeys.isEmpty())
		emit dataDump(tr("New keys"), tr("None"));
	else
	{
		//emit dataDump(tr("New keys"), m_newCentralKeys);

		QTextStream in(&m_newCentralKeys);

		QString line;
		while (in.readLineInto(&line))
		{
			emit dataDump(tr("New keys"), line);

			qApp->processEvents();  // GUI responsiveness
		}
	}

	qApp->processEvents();  // GUI responsiveness

	emit newKeysDownloaded(newCentralHc.count(), newCentralPk.count(), newCentralVuk.count());

	m_newLocalKeys.clear();
	QTextStream up(&m_newLocalKeys);

	if (!newLocalPk.isEmpty())
	{
		up << "\n";
	
		QSetIterator<QString> cpk(newLocalPk);
		while (cpk.hasNext())
		{
			up << m_localPk[cpk.next()] << "\n";

			qApp->processEvents();  // GUI responsiveness
		}
	}

	if (!newLocalHc.isEmpty())
	{
		up << "\n";

		QSetIterator<QString> chc(newLocalHc);
		while (chc.hasNext())
		{
			up << m_localHc[chc.next()] << "\n";

			qApp->processEvents();  // GUI responsiveness
		}
	}

	qApp->processEvents();  // GUI responsiveness

	if (!newLocalVuk.isEmpty())
	{
		up << "\n";

		QSetIterator<QString> cvuk(newLocalVuk);
		while (cvuk.hasNext())
		{
			up << m_localVuk[cvuk.next()] << "\n";

			qApp->processEvents();  // GUI responsiveness
		}
	}

	QSet<QString> knownVuk = QSet<QString>::fromList(m_centralVuk.keys()) + QSet<QString>::fromList(m_localVuk.keys());
	qApp->processEvents();  // GUI responsiveness

	int countLocalVuk = newLocalVuk.count();
	bool title = !newLocalVuk.isEmpty();

	QMapIterator<QString,QString> avuk(m_appDataVuk);
	while (avuk.hasNext())
	{
		qApp->processEvents();  // GUI responsiveness

		avuk.next();
		if (knownVuk.contains(avuk.key())) continue;

		if (!title)
		{
			up << "\n";
			title = true;
		}
	
		m_newLocalKeys.append(m_appDataVuk[avuk.key()] + "\n");
		++countLocalVuk;
		++m_newLocalKeysFound;
	}
	
	qApp->processEvents();  // GUI responsiveness

	if (m_newLocalKeys.isEmpty())
		emit dataDump(tr("Keys to upload"), tr("None"));
	else
	{
		//emit dataDump(tr("Keys to upload"), m_newLocalKeys);

		QTextStream in(&m_newLocalKeys);
		QString line;
		while (in.readLineInto(&line))
		{
			emit dataDump(tr("Keys to upload"), line);

			qApp->processEvents();  // GUI responsiveness
		}
	}

	emit newKeysToUpload(newLocalHc.count(), newLocalPk.count(), countLocalVuk);

	qApp->processEvents();  // GUI responsiveness

	m_newLocalKeys.prepend(QString(
		"\n"
		"; %1 %2\n"
		"; %3\n"
		";\n"
		"; keys uploaded to central database\n"
		"; processing keys: %4\n"
		"; host certificates: %5\n"
		"; disc VUK keys: %6\n")
		.arg(APPNAME).arg(KEYDB_SERVER_API).arg(QDateTime(QDateTime::currentDateTime()).toString("yyyy-MM-dd HH:mm:ss"))
		.arg(newLocalPk.count()).arg(newLocalHc.count()).arg(countLocalVuk));

	qApp->processEvents();  // GUI responsiveness

	startInstallNewKeys();
}

void AacsDatabaseObject::startInstallNewKeys()
{
	if (!m_newCentralKeysFound)
	{
		emit installedNewKeysEnded(0, true);
		return;
	}

	emit updateProcessingMessage(tr("Installing new AACS keys in KEYDB.CFG..."));

#if defined(WIN32)
	//QString appData = QString::fromUtf8(qgetenv("APPDATA"));
	QString appData;
	LPWSTR wszPath = NULL;
	HRESULT hr = SHGetKnownFolderPath(FOLDERID_RoamingAppData, KF_FLAG_CREATE, NULL, &wszPath);
	if (SUCCEEDED(hr))
	{
		appData = QString::fromWCharArray(wszPath);
	}
	else
	{
		appData = QString::fromUtf8(qgetenv("APPDATA"));
	}

	//if (wszPath) delete[] wszPath;

	if (!QDir(appData).mkpath("aacs"))
	{
		emit messageLog(tr("Install KEYDB.cfg: failed to create 'aacs' folder '%1'").arg(appData));
		emit installedNewKeysEnded(m_newCentralKeysFound, false);
		return;
	}

	QString localKeydb = appData + "\\aacs\\KEYDB.cfg";
	QString localBak = appData + "\\aacs\\KEYDB.bak";
	QFile localFile(localKeydb);
	
	QFile::remove(localBak);
	bool bak = QFile::copy(localKeydb, localBak);
	
	if (bak) 		
		emit messageLog(tr("Install KEYDB.cfg: old file copied to '%1'").arg(localBak));
	else
		emit messageLog(tr("Install KEYDB.cfg: failed to copy old file to '%1'").arg(localBak));

	qApp->processEvents();  // GUI responsiveness

	if (localFile.open(QIODevice::WriteOnly | QIODevice::Text))
	{
		QTextStream out(&localFile);
		QTextStream in(&m_updateResponse["KEYDB"]);
		out.setCodec("UTF-8");

		QString line;
		while (in.readLineInto(&line))
		{
			out << line << "\n";

			qApp->processEvents();  // GUI responsiveness
		}

		if (!m_newLocalKeys.isEmpty())
		{
			QTextStream in(&m_newLocalKeys);
			QString line;
			while (in.readLineInto(&line))
			{
				out << line << "\n";

				qApp->processEvents();  // GUI responsiveness
			}
		}

		localFile.close();

		qApp->processEvents();  // GUI responsiveness

		emit messageLog(tr("Install KEYDB.cfg: wrote new file '%1'").arg(localKeydb));
		emit dataDump(tr("KEYDB.cfg"), m_localKeydb);

		emit installedNewKeysEnded(m_newCentralKeysFound, true);
	}
	else
	{
		emit messageLog(tr("Install KEYDB.cfg: failed to open file for writing '%1'").arg(localKeydb));

		if (bak && QFile::rename(localBak, localKeydb))
			emit messageLog(tr("Install KEYDB.cfg: copied back from '%1'").arg(localBak));

		emit installedNewKeysEnded(m_newCentralKeysFound, false);
	}
#endif
}

void AacsDatabaseObject::startUploadNewKeys()
{
	if (m_netReply && m_netReply->isRunning()) return;  // once at a time

	m_updateResponse.clear();

	QNetworkAccessManager *accessManager = new QNetworkAccessManager(this);
	QNetworkConfigurationManager configurationManager;
	accessManager->setConfiguration(configurationManager.defaultConfiguration());
	if (accessManager->networkAccessible() != QNetworkAccessManager::Accessible)
	{
		// sounds to tough, we try to connect and we'll get a further error if not accessible
		//return;
	}

	QString dataToEncode = QString("a=%1&s=%2&p=%3&v=%4&d=%5&u=")
		.arg("upload").arg(KEYDB_SERVER_API).arg(KEYDB_ACCESS).arg(APPVERSION_STR).arg(0);

	QByteArray data = QUrl::toPercentEncoding(dataToEncode, "&=");  // space aren't + so might be %20 so NOT LIKE HTML FORMS
	data += m_newLocalKeys.toLocal8Bit().toPercentEncoding();

	QNetworkRequest request;
	request.setUrl(QUrl(KEYDB_SERVER));
	request.setRawHeader("Referer", QString("Updater %1").arg(APPVERSION_STR).toLatin1());
	request.setRawHeader("User-Agent", QString("AacsDatabaseObject").toLatin1());
	//request.setRawHeader("Content-Type", QString("application/x-www-form-urlencoded").toLatin1());
	request.setRawHeader("Content-Type", QString("application/x-www-form-urlencoded; charset=ibm852").toLatin1());

	emit messageLog(tr("Upload local keys: connecting to server"));
	
	connect(m_netTimer, SIGNAL(timeout()), SLOT(handleUploadReplyTimedOut()));
	m_netTimer->start(m_uploadTimeout);

	m_responseRaw.clear();	
	m_netReply = accessManager->post(request, data);

	connect(m_netReply, SIGNAL(error(QNetworkReply::NetworkError)), SLOT(handleUploadReplyError(QNetworkReply::NetworkError)));
	connect(m_netReply, SIGNAL(finished()), SLOT(handleUploadReplyFinished()));
	connect(m_netReply, SIGNAL(readyRead()), SLOT(handleUploadReplyRead()));
}

QByteArray AacsDatabaseObject::upgradeDownloadBin() const
{
	return m_updateResponse.value("FILEBIN", QByteArray());
}

QByteArray AacsDatabaseObject::upgradeDownloadBinUrl() const
{
	return m_updateResponse.value("URLBIN", QByteArray());
}

QByteArray AacsDatabaseObject::upgradeDownloadSrc() const
{
	return m_updateResponse.value("FILESRC", QByteArray());
}

QByteArray AacsDatabaseObject::upgradeDownloadSrcUrl() const
{
	return m_updateResponse.value("URLSRC", QByteArray());
}

bool AacsDatabaseObject::upgradeRequired() const
{
	if (m_updateResponse["UPDATE"] == "none") return false;
	if (m_updateResponse["UPDATE"] == "suggested") return false;

	return true;
}

bool AacsDatabaseObject::upgradeSuggested() const
{
	if (m_updateResponse["UPDATE"] == "suggested") return true;

	return false;
}
