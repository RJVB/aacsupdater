/*****************************************************************************
 *
 *  AacsUpdater.cpp -- Aacs Updater main program.
 * 
 *  Copyright (C) 2015 labDV - Starbuck <starbuck@labdv.com>
 *
 *  GNU General Public License Usage
 *  This file may be used under the terms of the GNU General Public 
 *  License version 3.0 as published by the Free Software Foundation
 *  and appearing in the file LICENSE.GPL included in the packaging
 *  of this file.  Please review the following information to ensure
 *  the GNU General Public License version 3.0 requirements will be
 *  met: http://www.gnu.org/copyleft/gpl.html.
 *
 *****************************************************************************/

#include "AacsUpdater.h"
#include "AacsUpdaterWindow.h"

#include <QApplication>
#include <QCommandLineParser>
#include <QMainWindow>
#include <QString>
#include <QStyleFactory>
#include <QSysInfo>

int main(int argc, char *argv[])
{
	QApplication app(argc, argv);

	Q_INIT_RESOURCE(AacsUpdater);

	if (QSysInfo::windowsVersion() > QSysInfo::WV_WINDOWS7)
		QApplication::setStyle(QStyleFactory::create("Fusion"));
	else
		QApplication::setStyle(QStyleFactory::create("Windows"));

	app.setApplicationDisplayName(APPNAME);
	app.setApplicationName(APPNAME);
	app.setOrganizationDomain(APPNAME);
	app.setOrganizationName("labDV.com");

	if (QSysInfo::windowsVersion() > QSysInfo::WV_WINDOWS7)
		app.setStyleSheet("* {font-family: Segoe UI, Verdana, Tahoma, Arial;}");

	app.setWindowIcon(QIcon(":/AacsUpdaterIcon.png"));

	QCommandLineParser parser;
	QCommandLineOption debugOption(QStringList() << "d" << "debug", QObject::tr("Debug mode."));
	parser.addOption(debugOption);
	parser.process(app);

	bool noDebug(true);
	noDebug = !parser.isSet(debugOption);
#ifdef _DEBUG 
	noDebug = false;
#endif

	AacsUpdaterWindow *mainWindow = new AacsUpdaterWindow(noDebug);	
	
	mainWindow->show();
	return app.exec();
}
