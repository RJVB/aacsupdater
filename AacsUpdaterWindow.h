/*****************************************************************************
 *
 *  AacsUpdaterWindow.h -- The Aacs Updater window.
 *
 *  Copyright (C) 2015 labDV - Starbuck <starbuck@labdv.com>
 *
 *  GNU General Public License Usage
 *  This file may be used under the terms of the GNU General Public 
 *  License version 3.0 as published by the Free Software Foundation
 *  and appearing in the file LICENSE.GPL included in the packaging
 *  of this file.  Please review the following information to ensure
 *  the GNU General Public License version 3.0 requirements will be
 *  met: http://www.gnu.org/copyleft/gpl.html.
 *
 *****************************************************************************/

#ifndef AACSUPDATERWINDOW_H
#define AACSUPDATERWINDOW_H

#include "AacsDatabaseObject.h"
#include "LogDialog.h"
#include "ProcessingWidget.h"

#include <QAction>
#include <QCloseEvent>
#include <QGroupBox>
#include <QLayout>
#include <QMainWindow>

class AacsUpdaterWindow : public QMainWindow
{
	Q_OBJECT

public:
	AacsUpdaterWindow(const bool noDebug, 
		QWidget *parent = 0, Qt::WindowFlags flags = 0);

signals:
	void	dataDump(QString,QString);
	void	messageLog(QString);

protected slots:	
	void	handleCheckApiEnded(bool);
	void	handleCheckApiTimedOut();
	void	handleFoundNewKeysDownloaded(int,int,int);
	void	handleFoundNewKeysToUpload(int,int,int);
	void	handleInstalledNewKeys(int,bool);
	void	handleLinkFromAppendDone(QString);
	void	handleLoadCentralKeydbEnded(bool);
	void	handleLoadCentralKeydbTimedOut();
	void	handleUploadNewKeysEnded(int,bool);
	void	handleUploadNewKeysTimedOut();
	void	updateAacsDatabase();

protected:
	void	appendCloseButton();
	void	appendDone(const QString &);
	void	appendFailed(const QString &);
	bool	appUiDesktopShrunk();
	void	appUiResetLayout();
	void	appUiRevertLayout();
	void	appUiSaveLayout();
	void	closeEvent(QCloseEvent *);
	void	createActions();
	void	createConnections();
	void	createLayout();
	void	createMenu();
	void	createWidgets();
	void	hideEvent(QHideEvent *);
	QGroupBox *upgradeBox(const QString &title, const QString &text, QWidget *parent=0);

protected:
	AacsDatabaseObject	*m_aacsDatabase;
	bool				m_aacsKeysInstalled;
	int					m_aacsKeysToInstall;
	LogDialog			*m_debugLog;
	ProcessingWidget	*m_processingWidget;

	// GUI
	QVBoxLayout *m_mainVLayout;
	QWidget		*m_mainWidget;
};

#endif
