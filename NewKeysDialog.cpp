/*****************************************************************************
 *
 *  NewKeysDialog.cpp -- Display new keys downloaded.
 * 
 *  Copyright (C) 2015 labDV - Starbuck <starbuck@labdv.com>
 *
 *  GNU General Public License Usage
 *  This file may be used under the terms of the GNU General Public 
 *  License version 3.0 as published by the Free Software Foundation
 *  and appearing in the file LICENSE.GPL included in the packaging
 *  of this file.  Please review the following information to ensure
 *  the GNU General Public License version 3.0 requirements will be
 *  met: http://www.gnu.org/copyleft/gpl.html.
 *
 *****************************************************************************/

#include "NewKeysDialog.h"

#include <QDesktopWidget>
#include <QApplication>
#include <QFontMetrics.h>
#include <QLayout>
#include <QSettings>
#include <QTimer>

NewKeysDialog::NewKeysDialog(QString title, QString text, QWidget *parent, Qt::WindowFlags flags)
	: QDialog (parent, flags)
{
	setWindowTitle(title.isEmpty() ? tr("New disc keys") : title);

	if (!flags)
		setWindowFlags(Qt::Tool);

	createWidgets();
	createLayout();
	createConnections();

	layoutLoadSettings();

	QString style = "font-family:monospace,courier new,courier; white-space:pre; color:black;";
	m_textDisplay->append("<span style='" + style + "'>" + text + "</span>");
}

void NewKeysDialog::closeEvent(QCloseEvent *ev)
{
	emit aboutToClose();
	ev->accept();
}

void NewKeysDialog::createConnections()
{
	connect(m_closeButton, SIGNAL(clicked()), SLOT(close()));

	connect(QApplication::desktop(), SIGNAL(screenCountChanged(int)), SLOT(restoreWindow()));
	connect(QApplication::desktop(), SIGNAL(resized(int)), SLOT(restoreWindow()));
}

void NewKeysDialog::createLayout()
{
	QHBoxLayout *controlLayout = new QHBoxLayout();
	controlLayout->addStretch(4);
	controlLayout->addWidget(m_closeButton);

	QVBoxLayout *mainLayout = new QVBoxLayout();
	mainLayout->addWidget(m_textDisplay);
	mainLayout->addLayout(controlLayout);

	setLayout(mainLayout);
}

void NewKeysDialog::createWidgets()
{
	m_textDisplay = new QTextBrowser(this);
	m_textDisplay->setLineWrapMode(QTextEdit::NoWrap);
	m_textDisplay->setReadOnly(true);

	QFont currentFont(m_textDisplay->font());
	currentFont.setFixedPitch(true);
	m_textDisplay->setFont(currentFont);

	m_closeButton = new QPushButton(tr("Close"));
	m_closeButton->setDefault(true);
}

void NewKeysDialog::layoutLoadSettings()
{
	QSettings settings;

	settings.beginGroup("Layout");
	settings.beginGroup("New disc keys");

	if (!settings.contains("size")) 
	{
		restoreWindow();
	}
	else
	{
		move(settings.value("position").toPoint());
		resize(settings.value("size").toSize());
	}

	settings.endGroup();
	settings.endGroup();
}

void NewKeysDialog::layoutSaveSettings()
{
	QSettings settings;

	settings.beginGroup("Layout");
	settings.beginGroup("New disc keys");

	settings.setValue("position", pos());
	settings.setValue("size", size());

	settings.endGroup();
	settings.endGroup();
}

void NewKeysDialog::moveEvent(QMoveEvent *ev)
{
	ev->accept();

	QTimer::singleShot(0, this, SLOT(layoutSaveSettings()));
}

void NewKeysDialog::resizeEvent(QResizeEvent *ev)
{
	ev->accept();

	QTimer::singleShot(0, this, SLOT(layoutSaveSettings()));
}

void NewKeysDialog::restoreWindow()
{
	int charHeight = m_textDisplay->fontMetrics().height();
	int charWidth = m_textDisplay->fontMetrics().averageCharWidth();

	setMinimumWidth(charWidth * 80);

	// Default size
	resize(charWidth * 60, charHeight * 33);

	// Extreme top left
	move(0, 0);
}
