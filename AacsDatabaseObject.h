/*****************************************************************************
 *
 *  AacsDatabaseObject.h -- Aacs Database processor (server API 1.0).
 *
 *  Copyright (C) 2015 labDV - Starbuck <starbuck@labdv.com>
 *
 *  GNU General Public License Usage
 *  This file may be used under the terms of the GNU General Public 
 *  License version 3.0 as published by the Free Software Foundation
 *  and appearing in the file LICENSE.GPL included in the packaging
 *  of this file.  Please review the following information to ensure
 *  the GNU General Public License version 3.0 requirements will be
 *  met: http://www.gnu.org/copyleft/gpl.html.
 *
 *****************************************************************************/

#ifndef AACSDATABASEOBJECT_H
#define AACSDATABASEOBJECT_H

#include <QByteArray>
#include <QObject>
#include <QSettings>
#include <QString>
#include <QStringList>
#include <QTimer>
#include <QtNetwork>

class AacsDatabaseObject : public QObject
{
	Q_OBJECT

public:
	AacsDatabaseObject(QObject *parent=0);
	~AacsDatabaseObject();
	
	QString	newDiscKeys() const;
	QString	newLocalKeys() const;
	void	startCheckApi(int);
	void	startFindNewKeys();
	void	startInstallNewKeys();
	void	startLoadCentralKeydb(int);
	void	startParseLocalConfig();
	void	startUploadNewKeys();
	QByteArray	upgradeDownloadBin() const;
	QByteArray	upgradeDownloadBinUrl() const;
	QByteArray	upgradeDownloadSrc() const;
	QByteArray	upgradeDownloadSrcUrl() const;
	bool	upgradeRequired() const;
	bool	upgradeSuggested() const;

signals:
	void	checkApiEnded(bool);
	void	checkApiTimedOut();
	void	dataDump(QString,QString);
	void	installedNewKeysEnded(int,bool);
	void	loadCentralKeydbEnded(bool);
	void	loadCentralKeydbTimedOut();
	void	messageLog(QString);
	void	newKeysDownloaded(int,int,int);
	void	newKeysToUpload(int,int,int);
	void	updateProcessingMessage(QString);
	void	uploadNewKeysEnded(int,bool);
	void	uploadNewKeysTimedOut();

protected slots:
	void	handleApiReplyError(QNetworkReply::NetworkError);
	void	handleApiReplyFinished();
	void	handleApiReplyRead();
	void	handleApiReplyTimedOut();
	void	handleCentralReplyError(QNetworkReply::NetworkError);
	void	handleCentralReplyFinished();
	void	handleCentralReplyRead();
	void	handleCentralReplyTimedOut();
	void	handleUploadReplyError(QNetworkReply::NetworkError);
	void	handleUploadReplyFinished();
	void	handleUploadReplyRead();
	void	handleUploadReplyTimedOut();

protected:
	void	abortNetworkConnection();
	void	parseApiReply();
	void	parseCentralReply();
	void	parseLocalKeydb();

protected:
	QMap<QString, QString> m_appDataVuk;
	QMap<QString, int> m_centralDisc;
	QMap<QString, QString> m_centralDiscName;
	QMap<QString, QStringList> m_centralDuplicateDiscName;
	QMap<QString, QString> m_centralHc;
	QMap<QString, QString> m_centralPk;
	QMap<QString, QString> m_centralVuk;
	QString			m_localKeydb;
	QMap<QString, int> m_localDisc;
	QMap<QString, QString> m_localDiscName;
	QMap<QString, QStringList> m_localDuplicateDiscName;
	QMap<QString, QString> m_localHc;
	QMap<QString, QString> m_localPk;
	QMap<QString, QString> m_localVuk;
	QString			m_newCentralKeys;
	int				m_newCentralKeysFound;
	QString			m_newDiscKeys;
	QString			m_newLocalKeys;
	int				m_newLocalKeysFound;
	QNetworkReply	*m_netReply;
	QTimer			*m_netTimer;
	QByteArray		m_responseRaw;
	int				m_uploadTimeout;

	QMap<QString, QByteArray> m_updateResponse;
};

#endif
