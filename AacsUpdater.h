/*****************************************************************************
 *
 *  AacsUpdater.h -- AacsUpdater main program.
 *
 *  Copyright (C) 2015 labDV - Starbuck <starbuck@labdv.com>
 *
 *  GNU General Public License Usage
 *  This file may be used under the terms of the GNU General Public 
 *  License version 3.0 as published by the Free Software Foundation
 *  and appearing in the file LICENSE.GPL included in the packaging
 *  of this file.  Please review the following information to ensure
 *  the GNU General Public License version 3.0 requirements will be
 *  met: http://www.gnu.org/copyleft/gpl.html.
 *
 *****************************************************************************/

#ifndef AACSUPDATER_H
#define AACSUPDATER_H

// Current version (1.major_version.minor_version.patch)
#define APPMAJORVERSION		"2.1"
#define APPVERSION_STR		"2.1.1.0"
#define APPVERSION			2,1,1,0

// Documentation
#define ONLINE_WEBSITE		"http://www.labdv.com/aacs/updater.php"

// Timing
#define CHECKAPI_TIMEOUT		30000
#define LOADCENTRALDB_TIMEOUT	600000  // could take time to download whole KEYDB.cfg file

// Your	application
#define APPNAME				"AACS Updater"
//#define KEYDB_ACCESS		"0000"
//#define KEYDB_SERVER		"http://www.labdv.com/aacs/keydb-server.php"
#include "../labDV.h"

#endif
