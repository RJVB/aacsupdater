; Installer script for AACS Updater
; Written by Starbuck (starbuck@users.sourceforge.net)
; Developed with Unicode NSIS 2.46;NSIS Modern User Interface

!include "Registry.nsh"
!include "x64.nsh"

; For AppAssocReg
!include /NONFATAL WinVer.nsh
!ifdef ___WINVER__NSH___
  RequestExecutionLevel user
!else
  !warning "Installer will be created without Vista compatibility.$\n            \
            Upgrade your NSIS installation to at least version 2.21 to resolve."
!endif

; Commented because useless Registry and Program Menu not cleaned up
RequestExecutionLevel admin
  
;--------------------------------
;Defines

  !define PRODUCT_PROJECT	"AacsUpdater" 
  !define PRODUCT_NAME		"AACS Updater" 
  
  !define PRODUCT_HEADER	".\${PRODUCT_PROJECT}.h" 
  !define PRODUCT_EXE		"AACS Updater.exe"
  !define PRODUCT_BUILD		".\${PRODUCT_PROJECT}BuildWin32.h"

  Var dllInstDir
  
;--------------------------------
;Version specific (extracted from source)
Section

  ;Var /GLOBAL PRODUCT_MAJORVERSION
  !searchparse /file ${PRODUCT_HEADER} `#define APPMAJORVERSION		"` PRODUCT_MAJORVERSION `"`
  DetailPrint ${PRODUCT_MAJORVERSION}
   
  ;Var /GLOBAL PRODUCT_VERSION
  !searchparse /file ${PRODUCT_HEADER} `#define APPVERSION_STR		"` PRODUCT_VERSION `"`
  DetailPrint ${PRODUCT_VERSION}
   
  ;Var /GLOBAL PRODUCT_BUILD
  !searchparse /file ${PRODUCT_BUILD} "#define BUILDNUMBER " PRODUCT_BUILD
  DetailPrint ${PRODUCT_BUILD}

 SectionEnd
 
;--------------------------------
;Defines more

  !define PRODUCT_APP_PATHS_KEY "Software\Microsoft\Windows\CurrentVersion\App Paths\${PRODUCT_NAME}"
  !define PRODUCT_REG_KEY "Software\labDV\${PRODUCT_NAME}"

  !define PRODUCT_UNINST_EXE "Uninstall.exe"
  !define PRODUCT_UNINST_KEY "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_NAME}"

;--------------------------------
;Include Modern UI

  !include "MUI2.nsh"

;--------------------------------
;Interface Configuration

  ;Installer/Uninstaller icons
  !define MUI_ICON "${NSISDIR}\Contrib\Graphics\Icons\orange-install.ico"
  !define MUI_UNICON "${NSISDIR}\Contrib\Graphics\Icons\orange-uninstall.ico"
  
  ; Misc.
  !define MUI_WELCOMEFINISHPAGE_BITMAP "${NSISDIR}\Contrib\Graphics\Wizard\orange.bmp"
  !define MUI_UNWELCOMEFINISHPAGE_BITMAP "${NSISDIR}\Contrib\Graphics\Wizard\orange-uninstall.bmp"
  
  !define MUI_HEADERIMAGE
  !define MUI_HEADERIMAGE_BITMAP "${NSISDIR}\Contrib\Graphics\Header\orange.bmp" ; optional
  !define MUI_ABORTWARNING

  ; License page
  !define MUI_LICENSEPAGE_RADIOBUTTONS
  
  ; Finish page
  !define MUI_FINISHPAGE_LINK "AACS Updater home page"
  !define MUI_FINISHPAGE_LINK_LOCATION "http://www.labdv.com/aacs/updater.php"
  !define MUI_FINISHPAGE_NOREBOOTSUPPORT
  !define MUI_FINISHPAGE_RUN "$INSTDIR\${PRODUCT_EXE}"
  ;!define MUI_FINISHPAGE_RUN_NOTCHECKED
  
;--------------------------------
;System

  !define CSIDL_APPDATA '0x1A' ;Application Data path
  !define CSIDL_PROGRAMFILES '0x26' ;Program Files path

  Function ".onInit"

  ;${If} ${RunningX64}
  ;      MessageBox MB_OK|MB_ICONINFORMATION "Please install OpenVUK plugin for Windows 64bit"
  ;      Quit
  ;${EndIf}

  System::Call 'shell32::SHGetSpecialFolderPath(i $HWNDPARENT, t .r1, i ${CSIDL_PROGRAMFILES}, i0)i.r0'
  StrCpy $dllInstDir "$1"
  IfFileExists "$1\VideoLAN\VLC\vlc.exe" VlcExists VlcNotFound
VlcExists:
  StrCpy $dllInstDir "$dllInstDir\VideoLAN\VLC"
VlcNotFound:  
  
  FunctionEnd

;--------------------------------
;Pages

  !insertmacro MUI_PAGE_WELCOME
  !insertmacro MUI_PAGE_LICENSE "..\src\LICENSE.GPL3"
  ;!insertmacro MUI_PAGE_COMPONENTS
  ;!insertmacro MUI_PAGE_DIRECTORY

  ;Select MPlayer/VLC directory 
  !define MUI_PAGE_HEADER_SUBTEXT "Select AACS/BD+ DLLs installation folder (usually MPlayer or VLC folder)."
  !define MUI_DIRECTORYPAGE_TEXT_TOP "AACS/BD+ DLLs (libaacs.dll and libbdplus.dll) will be installed in the following folder. To install in a different folder, click Browse and select another folder. Click Next to continue."
  !define MUI_DIRECTORYPAGE_VARIABLE $dllInstDir ; <= the other directory will be stored into that variable
  !insertmacro MUI_PAGE_DIRECTORY
  !insertmacro MUI_PAGE_INSTFILES
  !insertmacro MUI_PAGE_FINISH
  !insertmacro MUI_UNPAGE_WELCOME
  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_INSTFILES
  !insertmacro MUI_UNPAGE_FINISH

;--------------------------------
;Languages

  !insertmacro MUI_LANGUAGE "English"

;--------------------------------
;General

  !define INSTALLER_NAME "aacs-updater-${PRODUCT_VERSION}-x64-FULL-setup.exe"
 
  VIProductVersion ${PRODUCT_VERSION}

  VIAddVersionKey /LANG=1033-English "ProductName" "${PRODUCT_NAME}"
  VIAddVersionKey /LANG=1033-English "CompanyName" "labDV"
  VIAddVersionKey /LANG=1033-English "InternalName" "${PRODUCT_NAME}"
  VIAddVersionKey /LANG=1033-English "LegalTrademarks" "${PRODUCT_NAME} is a trademark of labDV company"
  VIAddVersionKey /LANG=1033-English "LegalCopyright" "labDV � 2014-2015"
  VIAddVersionKey /LANG=1033-English "OriginalFilename" "${INSTALLER_NAME}"
  VIAddVersionKey /LANG=1033-English "FileDescription" "${PRODUCT_NAME} Installer"
  VIAddVersionKey /LANG=1033-English "FileVersion" ${PRODUCT_VERSION}
  VIAddVersionKey /LANG=1033-English "ProductVersion" "${PRODUCT_VERSION} (build ${PRODUCT_BUILD})"

  ;Name and file
  Name "${PRODUCT_NAME}"
  BrandingText "${PRODUCT_NAME} (version ${PRODUCT_MAJORVERSION} 64-bit) setup for Windows"
  OutFile "..\${INSTALLER_NAME}"

  ;Default installation folder
  InstallDir "$PROGRAMFILES64\${PRODUCT_NAME}"

;--------------------------------
;Uninstall previous version
; The "" makes the section hidden. 
; This section is executed silently after user has made all choices (just before copying files)
Section "" SecUninstallPrevious
  Call UninstallPrevious
SectionEnd

Function UninstallPrevious

  ; Check for uninstaller.
  ReadRegStr $R0 HKLM "${PRODUCT_UNINST_KEY}" "UninstallString"
  StrCmp $R0 "" Done

  ; I prefer silent uninstall
  ;MessageBox MB_OKCANCEL|MB_ICONEXCLAMATION \
  ;"${PRODUCT_DESCRIPTION} is already installed. $\n$\nClick `OK` to remove the \
  ;previous version or `Cancel` to cancel this upgrade." \
  ;IDOK Uninst
  ;Abort
  
  ;Uninst:
  ; Run the uninstaller silently.
  ClearErrors
  ExecWait '$R0 /S _?=$INSTDIR' ;Do not copy the uninstaller to a temp file

  Done:
  
FunctionEnd

;--------------------------------
;Installer Sections

Section
 
  SetOutPath "$INSTDIR"
  File /a "..\..\x64\Release\${PRODUCT_EXE}"
  File /a /r "..\..\..\..\bin\qt\qt5-x64\*.*"
  File /a /r "..\..\..\..\bin\qt\qt5-plugins-x64\*.*"
  
  ; DLLs for VLC
  SetOutPath $dllInstDir
  File /a "..\..\..\..\bin\bluray\win64\libaacs.dll"
  ;File /a "..\..\..\..\bin\bluray\win64\libbdplus.dll"
  
  ; AACS and BD+ conf
  SetOutPath "$APPDATA\aacs"
  File /nonfatal /a "..\..\..\..\bin\bluray\KEYDB.cfg"
  ;SetOutPath "$APPDATA\bdplus"
  ;File /nonfatal /a /r "..\..\..\..\bin\bluray\vm0"

  ;Store installation folder
  WriteRegStr HKLM "${PRODUCT_UNINST_KEY}" "InstallDir" $INSTDIR

  ;Create uninstaller
  WriteUninstaller "$INSTDIR\Uninstall.exe"
  
  ;Write the uninstall keys for Windows
  WriteRegStr HKLM "${PRODUCT_UNINST_KEY}" "DisplayName" "${PRODUCT_NAME}"
  WriteRegStr HKLM "${PRODUCT_UNINST_KEY}" "DisplayIcon" "$INSTDIR\${PRODUCT_NAME}"
  WriteRegStr HKLM "${PRODUCT_UNINST_KEY}" "DisplayVersion" "${PRODUCT_MAJORVERSION}"
  WriteRegStr HKLM "${PRODUCT_UNINST_KEY}" "HelpLink" "http://www.labdv.com/aacs/updater.php"
  WriteRegStr HKLM "${PRODUCT_UNINST_KEY}" "Publisher" "labDV"
  WriteRegStr HKLM "${PRODUCT_UNINST_KEY}" "UninstallString" "$INSTDIR\Uninstall.exe"
  WriteRegStr HKLM "${PRODUCT_UNINST_KEY}" "URLInfoAbout" "http://www.labdv.com/aacs/updater.php"
  WriteRegStr HKLM "${PRODUCT_UNINST_KEY}" "URLUpdateInfo" "http://www.labdv.com/aacs/updater.php"
  WriteRegDWORD HKLM "${PRODUCT_UNINST_KEY}" "NoModify" "1"
  WriteRegDWORD HKLM "${PRODUCT_UNINST_KEY}" "NoRepair" "1"
  
  ;Program start menu
  CreateDirectory "$SMPROGRAMS\${PRODUCT_NAME}"
  CreateShortCut "$SMPROGRAMS\${PRODUCT_NAME}\${PRODUCT_NAME}.lnk" "$INSTDIR\${PRODUCT_EXE}"
  CreateShortCut "$SMPROGRAMS\${PRODUCT_NAME}\${PRODUCT_NAME} --debug.lnk" "$INSTDIR\${PRODUCT_EXE}" -d
  CreateShortCut "$SMPROGRAMS\Uninstall ${PRODUCT_NAME}.lnk" "$INSTDIR\Uninstall.exe"
  
  ;Desktop shortcut
  CreateShortCut "$DESKTOP\${PRODUCT_NAME}.lnk" "$INSTDIR\${PRODUCT_EXE}" 

sectionEnd

;--------------------------------
;Uninstaller Section

Section "Uninstall"

  Delete "$INSTDIR\accessible\*.*"
  Delete "$INSTDIR\iconengines\*.*"
  Delete "$INSTDIR\imageformats\*.*"
  Delete "$INSTDIR\platforms\*.*"
  
  Delete "$INSTDIR\*.*"
  
  Delete "$SMPROGRAMS\${PRODUCT_NAME}\${PRODUCT_NAME} Updater.lnk"
  Delete "$SMPROGRAMS\${PRODUCT_NAME}\${PRODUCT_NAME}.lnk"
  Delete "$SMPROGRAMS\${PRODUCT_NAME}\${PRODUCT_NAME} --debug.lnk"
  Delete "$SMPROGRAMS\Uninstall ${PRODUCT_NAME}.lnk"
  Delete "$DESKTOP\${PRODUCT_NAME}.lnk" 
  
  RMDir "$INSTDIR\accessible"
  RMDir "$INSTDIR\iconengines"
  RMDir "$INSTDIR\imageformats"
  RMDir "$INSTDIR\platforms"

  RMDir "$INSTDIR"
  
  RMDir "$SMPROGRAMS\${PRODUCT_NAME}"  # not working!

  DeleteRegKey HKCU "${PRODUCT_REG_KEY}"  # not working!

  ;SetRegView 32
  DeleteRegKey HKLM "${PRODUCT_REG_KEY}"
  DeleteRegKey HKLM "${PRODUCT_UNINST_KEY}"
  
SectionEnd
