/*****************************************************************************
 *
 *  ProcessingWidget.cpp -- Animated widget to show processing status.
 *
 *  Copyright (C) 2015 labDV - Starbuck <starbuck@labdv.com>
 *
 *  GNU General Public License Usage
 *  This file may be used under the terms of the GNU General Public 
 *  License version 3.0 as published by the Free Software Foundation
 *  and appearing in the file LICENSE.GPL included in the packaging
 *  of this file.  Please review the following information to ensure
 *  the GNU General Public License version 3.0 requirements will be
 *  met: http://www.gnu.org/copyleft/gpl.html.
 *
 *****************************************************************************/

#include "ProcessingWidget.h"

#include <QPixmap>
#include <QTimer>

ProcessingWidget::ProcessingWidget(const QString &animation, const QSize &size, const int speed,
	const QString &text, const QString &style, QWidget *parent)
	: QWidget(parent)
{
	setWindowFlags(Qt::SplashScreen);
	setContentsMargins(5,5,5,5);
	setStyleSheet(QString("QLabel {%1}").arg(style));

	m_animationMovie = new QMovie(animation);
	m_animationMovie->setScaledSize(size);
	m_animationMovie->setSpeed(speed);

	m_animationLabel = new QLabel();
	m_animationLabel->setMovie(m_animationMovie);
	m_animationLabel->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

	m_infoLabel = new QLabel(text);
	m_infoLabel->setOpenExternalLinks(true);
	
	QHBoxLayout *hboxLayout = new QHBoxLayout(this);
	hboxLayout->addWidget(m_animationLabel, Qt::AlignCenter);
	hboxLayout->addWidget(m_infoLabel);
}

void ProcessingWidget::setAnimation(const QString &path, const QSize size, const int speed)
{
	if (!m_animationMovie)
	{
		m_animationMovie = new QMovie(path);
		m_animationMovie->setScaledSize(size);
		m_animationMovie->setSpeed(speed);

		m_animationLabel->setMovie(m_animationMovie);
	}
	else
	{	
		int state = m_animationMovie->state();

		m_animationMovie->stop();
		m_animationMovie->setFileName(path);
		m_animationMovie->setScaledSize(size);
		m_animationMovie->setSpeed(speed);
			
		if (state == QMovie::Running) m_animationMovie->start();
	}
}

void ProcessingWidget::setImage(const QString &path, const QSize size)
{
	m_animationLabel->setMovie(0);
	if (m_animationMovie) delete(m_animationMovie);
	m_animationMovie = 0;

	QPixmap pix(path);
	pix = pix.scaled(size, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
	
	m_animationLabel->setPixmap(pix);
}

void ProcessingWidget::setText(const QString &message, const QString &style)
{
	m_infoLabel->setText(message);

	if (!style.isEmpty())
		setStyleSheet(QString("QLabel {%1}").arg(style));
}

void ProcessingWidget::start(QString message)
{
	if (!message.isEmpty()) m_infoLabel->setText(message);

	m_animationMovie->start();
}

void ProcessingWidget::stop(QString message)
{
	if (!message.isEmpty()) m_infoLabel->setText(message);

	m_animationMovie->stop();
}

