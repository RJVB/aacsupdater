/*****************************************************************************
 *
 *  LogDialog.cpp -- Display log events.
 * 
 *  Copyright (C) 2015 labDV - Starbuck <starbuck@labdv.com>
 *
 *  GNU General Public License Usage
 *  This file may be used under the terms of the GNU General Public 
 *  License version 3.0 as published by the Free Software Foundation
 *  and appearing in the file LICENSE.GPL included in the packaging
 *  of this file.  Please review the following information to ensure
 *  the GNU General Public License version 3.0 requirements will be
 *  met: http://www.gnu.org/copyleft/gpl.html.
 *
 *****************************************************************************/

#include "LogDialog.h"

#include <QDesktopWidget>
#include <QApplication>
#include <QFontMetrics.h>
#include <QLayout>
#include <QSettings>
#include <QTimer>

LogDialog::LogDialog(QString title, QWidget *parent, Qt::WindowFlags flags)
	: QDialog (parent, flags)
{
	m_title = title.isEmpty() ? tr("Log") : title;
	setWindowTitle(m_title);

	if (!flags)
		setWindowFlags(Qt::Tool);

	createWidgets();
	createLayout();
	createConnections();

	m_styleDump = "font-family:monospace,courier new,courier; white-space:pre; color:blue;";
	m_styleMessage = "font-family:monospace,courier new,courier; white-space:pre; color:black;";

	layoutLoadSettings();
}

void LogDialog::addDump(QString title, QString data)
{
	QTextBrowser *dumpDisplay = m_dumpDisplay.value(title, 0);
	
	if (!dumpDisplay)
	{
		dumpDisplay = new QTextBrowser(this);
		dumpDisplay->setLineWrapMode(QTextEdit::NoWrap);
		dumpDisplay->setReadOnly(true);
		dumpDisplay->setOpenLinks(false);  // avoid scrolling when click console anchor
		dumpDisplay->setFrameShape(QFrame::NoFrame);

		QFont currentFont(dumpDisplay->font());
		currentFont.setFixedPitch(true);
		dumpDisplay->setFont(currentFont);

		m_tabWidget->addTab(dumpDisplay, title);

		m_dumpDisplay[title] = dumpDisplay;
	}

	dumpDisplay->append("<span  style='" + m_styleDump + "'>" + data + "</span>"); // UTF8 chars not dispalyed correctly but ok in the data QString
}

void LogDialog::addMessage(QString text)
{
	m_messageDisplay->append("<span style='" + m_styleMessage + "'>" + text + "</span>");
}

void LogDialog::closeEvent(QCloseEvent *ev)
{
	emit aboutToClose();
	ev->accept();
}

void LogDialog::connectClient(QObject *client)
{
	connect(client, SIGNAL(dataDump(QString,QString)), SLOT(addDump(QString,QString)));
	connect(client, SIGNAL(messageLog(QString)), SLOT(addMessage(QString)));
}

void LogDialog::createConnections()
{
	connect(QApplication::desktop(), SIGNAL(screenCountChanged(int)), SLOT(restoreWindow()));
	connect(QApplication::desktop(), SIGNAL(resized(int)), SLOT(restoreWindow()));
}

void LogDialog::createLayout()
{
	QVBoxLayout *mainLayout = new QVBoxLayout();
	mainLayout->addWidget(m_tabWidget);

	setLayout(mainLayout);
}

void LogDialog::createWidgets()
{
	m_messageDisplay = new QTextBrowser(this);
	m_messageDisplay->setLineWrapMode(QTextEdit::NoWrap);
	m_messageDisplay->setReadOnly(true);
	m_messageDisplay->setOpenLinks(false);  // avoid scrolling when click console anchor
	m_messageDisplay->setFrameShape(QFrame::NoFrame);

	QFont currentFont(m_messageDisplay->font());
	currentFont.setFixedPitch(true);
	m_messageDisplay->setFont(currentFont);

	m_tabWidget = new QTabWidget(this);
	m_tabWidget->addTab(m_messageDisplay, tr("Log"));
}

void LogDialog::layoutLoadSettings()
{
	QString logKeyReg = m_title.replace('/','-');

	QSettings settings;

	settings.beginGroup("Layout");
	settings.beginGroup(logKeyReg);

	if (!settings.contains("size")) 
	{
		restoreWindow();
	}
	else
	{
		move(settings.value("position").toPoint());
		resize(settings.value("size").toSize());
	}

	settings.endGroup();
	settings.endGroup();
}

void LogDialog::layoutSaveSettings()
{
	QString logKeyReg = m_title.replace('/','-');

	QSettings settings;

	settings.beginGroup("Layout");
	settings.beginGroup(logKeyReg);

	settings.setValue("position", pos());
	settings.setValue("size", size());

	settings.endGroup();
	settings.endGroup();
}

void LogDialog::moveEvent(QMoveEvent *ev)
{
	ev->accept();

	QTimer::singleShot(0, this, SLOT(layoutSaveSettings()));
}

void LogDialog::resizeEvent(QResizeEvent *ev)
{
	ev->accept();

	QTimer::singleShot(0, this, SLOT(layoutSaveSettings()));
}

void LogDialog::restoreWindow()
{
	int charHeight = m_messageDisplay->fontMetrics().height();
	int charWidth = m_messageDisplay->fontMetrics().averageCharWidth();

	setMinimumWidth(charWidth * 80);

	// Default size
	resize(charWidth * 60, charHeight * 33);

	// Extreme top left
	move(0, 0);
}
