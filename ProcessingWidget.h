/*****************************************************************************
 *
 *  ProcessingWidget.h -- Animated widget to show processing status.
 *
 *  Copyright (C) 2015 labDV - Starbuck <starbuck@labdv.com>
 *
 *  GNU General Public License Usage
 *  This file may be used under the terms of the GNU General Public 
 *  License version 3.0 as published by the Free Software Foundation
 *  and appearing in the file LICENSE.GPL included in the packaging
 *  of this file.  Please review the following information to ensure
 *  the GNU General Public License version 3.0 requirements will be
 *  met: http://www.gnu.org/copyleft/gpl.html.
 *
 *****************************************************************************/

#ifndef COMMON_PROCESSMONITORWIDGET
#define COMMON_PROCESSMONITORWIDGET

#include <QWidget>
#include <QLayout>
#include <QLabel>
#include <QMovie>

class ProcessingWidget : public QWidget
{
	Q_OBJECT

public:
	ProcessingWidget(const QString &animation, const QSize &size, const int speed,
		const QString &text, const QString &style, QWidget *parent=0);
	//~ProcessingWidget();

	void setAnimation(const QString &path, const QSize size, const int speed);
	void setImage(const QString &path, const QSize size);
	void setText(const QString &message, const QString &style=QString());

public slots:
	void start(QString message=QString());
	void stop(QString message=QString());

protected:
	QLabel		*m_animationLabel;
	QMovie		*m_animationMovie;
	QLabel		*m_infoLabel;
};

#endif
