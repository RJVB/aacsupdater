/*****************************************************************************
 *
 *  LogDialog.h -- Display log events.
 *
 *  Copyright (C) 2015 labDV - Starbuck <starbuck@labdv.com>
 *
 *  GNU General Public License Usage
 *  This file may be used under the terms of the GNU General Public 
 *  License version 3.0 as published by the Free Software Foundation
 *  and appearing in the file LICENSE.GPL included in the packaging
 *  of this file.  Please review the following information to ensure
 *  the GNU General Public License version 3.0 requirements will be
 *  met: http://www.gnu.org/copyleft/gpl.html.
 *
 *****************************************************************************/

#ifndef LOGDIALOG_H
#define LOGDIALOG_H

#include <QCloseEvent>
#include <QDialog>
#include <QHash>
#include <QPushButton>
#include <QSettings>
#include <QString>
#include <QStringList>
#include <QTabWidget>
#include <QTextBrowser>

class LogDialog : public QDialog
{
	Q_OBJECT

public:
	LogDialog(QString title=QString(), QWidget *parent=0, Qt::WindowFlags flags=0);
	//~LogDialog();
	
	void connectClient(QObject *);

signals:
	void aboutToClose();

public slots:
	void addDump(QString,QString);
	void addMessage(QString);
	void restoreWindow();

protected slots:
	void layoutSaveSettings();

protected:
	void closeEvent(QCloseEvent *);

	void createConnections();
	void createLayout();
	void createWidgets();

	void layoutLoadSettings();
	void moveEvent(QMoveEvent *);
	void resizeEvent(QResizeEvent *);

protected:
	QString m_styleDump;
	QString m_styleMessage;
	QString	m_title;

	QHash<QString, QTextBrowser*> m_dumpDisplay;

	// GUI
	QTextBrowser	*m_messageDisplay;
	QTabWidget		*m_tabWidget;
};

#endif
