/*****************************************************************************
 *
 *  NewKeysDialog.h -- Display new keys downloaded.
 *
 *  Copyright (C) 2015 labDV - Starbuck <starbuck@labdv.com>
 *
 *  GNU General Public License Usage
 *  This file may be used under the terms of the GNU General Public 
 *  License version 3.0 as published by the Free Software Foundation
 *  and appearing in the file LICENSE.GPL included in the packaging
 *  of this file.  Please review the following information to ensure
 *  the GNU General Public License version 3.0 requirements will be
 *  met: http://www.gnu.org/copyleft/gpl.html.
 *
 *****************************************************************************/

#ifndef NEWKEYSDIALOG_H
#define NEWKEYSDIALOG_H

#include <QCloseEvent>
#include <QDialog>
#include <QPushButton>
#include <QSettings>
#include <QString>
#include <QStringList>
#include <QTextBrowser>

class NewKeysDialog : public QDialog
{
	Q_OBJECT

public:
	NewKeysDialog(QString title, QString text, QWidget *parent = 0, Qt::WindowFlags flags = 0);
	//~NewKeysDialog();
	
signals:
	void aboutToClose();

public slots:
	void restoreWindow();

protected slots:
	void layoutSaveSettings();

protected:
	void closeEvent(QCloseEvent *);

	void createConnections();
	void createLayout();
	void createWidgets();

	void layoutLoadSettings();
	void moveEvent(QMoveEvent *);
	void resizeEvent(QResizeEvent *);

protected:
	// GUI
	QTextBrowser	*m_textDisplay;
	QPushButton		*m_closeButton;
};

#endif
