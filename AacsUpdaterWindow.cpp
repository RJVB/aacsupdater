/*****************************************************************************
 *
 *  AacsUpdaterWindow.cpp -- The Aacs Updater window.
 *
 *  Copyright (C) 2015 labDV - Starbuck <starbuck@labdv.com>
 *
 *  GNU General Public License Usage
 *  This file may be used under the terms of the GNU General Public 
 *  License version 3.0 as published by the Free Software Foundation
 *  and appearing in the file LICENSE.GPL included in the packaging
 *  of this file.  Please review the following information to ensure
 *  the GNU General Public License version 3.0 requirements will be
 *  met: http://www.gnu.org/copyleft/gpl.html.
 *
 *****************************************************************************/

#include "AacsUpdater.h"
#include "AacsUpdaterWindow.h"
#include "NewKeysDialog.h"

#include <QApplication>
#include <QDebug>
#include <QDesktopServices>
#include <QDesktopWidget>
#include <QDir>
#include <QPushButton>
#include <QSettings>
#include <QStandardPaths>
#include <QTimer>

#if defined(WIN32)
#include <windows.h>
#endif

AacsUpdaterWindow::AacsUpdaterWindow(const bool noDebug, 
	QWidget *parent, Qt::WindowFlags flags)
	: QMainWindow(parent, flags)
{
	m_aacsKeysInstalled = false;
	m_aacsKeysToInstall = 0;

	setAttribute(Qt::WA_AlwaysShowToolTips);	
	//setAttribute(Qt::WA_DeleteOnClose);	
	//setMouseTracking(true);

	m_debugLog = new LogDialog(tr("Messages"), this);
	m_debugLog->connectClient(this);
	if (!noDebug) m_debugLog->show();

	m_aacsDatabase = new AacsDatabaseObject(this);
	m_debugLog->connectClient(m_aacsDatabase);

	if (appUiDesktopShrunk()) appUiResetLayout();
	appUiRevertLayout();

	createActions();
	createMenu();
	createWidgets();
	createConnections();
	createLayout();

	m_processingWidget = new ProcessingWidget(":/processing.gif", QSize(43,11), 100,
		tr("Starting database update..."), "font-size: 14px; color: #149dd6", this);
	
	m_mainVLayout->addWidget(m_processingWidget);
	m_mainVLayout->addStretch();
	m_mainVLayout->insertSpacing(m_mainVLayout->count(), 8);

	connect(m_aacsDatabase, SIGNAL(updateProcessingMessage(QString)), m_processingWidget, SLOT(start(QString)));

	m_processingWidget->start();

	QTimer::singleShot(0, this, SLOT(updateAacsDatabase()));
	//--> send API version and get OK or upgrade link 

#if defined(WIN32)
/*
	HWND hWnd = (HWND)winId();
	HMENU mnu = GetSystemMenu(hWnd, false);

	MENUITEMINFO info = { sizeof(MENUITEMINFO) };
	
	GetMenuItemInfo(mnu, SC_CLOSE, false, &info);
	TCHAR name[256] = L"Close";
	info.fMask = MIIM_STRING;
	info.dwTypeData = name;
	info.cch = sizeof(name) / sizeof(TCHAR);
	ModifyMenu(mnu, SC_CLOSE, MF_STRING, 0, info.dwTypeData);

	GetMenuItemInfo(mnu, SC_RESTORE, false, &info);
	TCHAR name2[256] = L"Restore";
	info.fMask = MIIM_STRING;
	info.dwTypeData = name2;
	info.cch = sizeof(name2) / sizeof(TCHAR);
	ModifyMenu(mnu, SC_RESTORE, MF_STRING, 0, info.dwTypeData);

	DeleteMenu(mnu, SC_CLOSE, MF_BYCOMMAND);
	DeleteMenu(mnu, SC_MOVE, MF_BYCOMMAND);
	DeleteMenu(mnu, SC_MAXIMIZE, MF_BYCOMMAND);
	DeleteMenu(mnu, SC_MINIMIZE, MF_BYCOMMAND);
	DeleteMenu(mnu, SC_RESTORE, MF_BYCOMMAND);
	DeleteMenu(mnu, SC_SIZE, MF_BYCOMMAND);*/
#endif
}

void AacsUpdaterWindow::appendCloseButton()
{
	QPushButton *closeButton = new QPushButton(tr("Close"), this);

	closeButton->setStyleSheet(QString("QPushButton{background-color:#333333; color:white;"
										"border:none; border-radius:5px;"
										"font-weight:bold; padding:8px}"
										"QPushButton:hover{background-color:#828282;}"));
	closeButton->setMinimumWidth(60);

	connect(closeButton, SIGNAL(released()), SLOT(close()));
	
	QHBoxLayout *closeLayout = new QHBoxLayout();
	closeLayout->setAlignment(Qt::AlignCenter);
	closeLayout->addWidget(closeButton);

	m_mainVLayout->insertSpacing(m_mainVLayout->count() - 2, 24);
	m_mainVLayout->insertWidget(m_mainVLayout->count() - 1, closeButton, 0, Qt::AlignCenter);
}

void AacsUpdaterWindow::appendDone(const QString &text)
{
	QPixmap pix(":/done.png");
	pix = pix.scaled(QSize(16,16), Qt::IgnoreAspectRatio, Qt::SmoothTransformation);

	QLabel *imgLabel = new QLabel();
	imgLabel->setPixmap(pix);

	QLabel *txtLabel = new QLabel();
	txtLabel->setText(text);
	txtLabel->setOpenExternalLinks(false);
	txtLabel->setTextInteractionFlags(txtLabel->textInteractionFlags()|Qt::LinksAccessibleByMouse);
	connect(txtLabel, SIGNAL(linkActivated(QString)), SLOT(handleLinkFromAppendDone(QString)));

	QHBoxLayout *hLayout = new QHBoxLayout();
	hLayout->addSpacing(24);
	hLayout->addWidget(imgLabel, 0 , Qt::AlignLeft|Qt::AlignVCenter);
	hLayout->addWidget(txtLabel, 0 , Qt::AlignLeft|Qt::AlignVCenter);
	hLayout->addStretch(2);

	m_mainVLayout->insertLayout(m_mainVLayout->count() - 2, hLayout);
}

void AacsUpdaterWindow::appendFailed(const QString &text)
{
	QPixmap pix(":/failed.png");
	pix = pix.scaled(QSize(16,16), Qt::IgnoreAspectRatio, Qt::SmoothTransformation);

	QLabel *imgLabel = new QLabel();
	imgLabel->setPixmap(pix);

	QLabel *txtLabel = new QLabel();
	txtLabel->setText(text);

	QHBoxLayout *hLayout = new QHBoxLayout();
	hLayout->addSpacing(24);
	hLayout->addWidget(imgLabel, 0 , Qt::AlignLeft|Qt::AlignVCenter);
	hLayout->addWidget(txtLabel, 0 , Qt::AlignLeft|Qt::AlignVCenter);
	hLayout->addStretch(2);

	m_mainVLayout->insertLayout(m_mainVLayout->count() - 2, hLayout);
}

bool AacsUpdaterWindow::appUiDesktopShrunk()
{
	QSettings RegApp;
	RegApp.beginGroup("Desktop");

	if (!RegApp.contains("screens")) return false;

	// Number of screens has changed? (since latest session)
	if (RegApp.value("screens").toInt() != QApplication::desktop()->screenCount())
		return true;
	else
		for (int i = 0; i < QApplication::desktop()->screenCount(); i++)
		{
			RegApp.beginGroup(QString("Screen %1").arg(i));

			QRect currentGeometry = QApplication::desktop()->availableGeometry(i);
			QRect previousGeometry = RegApp.value("size").toRect();
			
			if (currentGeometry.width() < previousGeometry.width()
				|| currentGeometry.height() < previousGeometry.height())
			{
				return true;
			}

			RegApp.endGroup();
		}

	return false;
}

void AacsUpdaterWindow::appUiResetLayout()
{
	m_debugLog->restoreWindow();

	// Default size
	resize(360, 100);

	// Near top left
	move(10, 10);
}

void AacsUpdaterWindow::appUiRevertLayout()
{
	QSettings RegApp;

	if (!RegApp.contains("Layout/Main/position")) 
	{
		appUiResetLayout();
	}
	else
	{
		move(RegApp.value("Layout/Main/position").toPoint());
	}

	resize(360, 100);
}

void AacsUpdaterWindow::appUiSaveLayout()
{
	QSettings RegApp;

	// Main window layout
	RegApp.beginGroup("Layout");
	RegApp.beginGroup("Main");

	RegApp.setValue("position", pos());

	RegApp.endGroup();
	RegApp.endGroup();

	// Desktop layout
	RegApp.beginGroup("Desktop");
	
	RegApp.remove("");
	RegApp.setValue("screens", QApplication::desktop()->screenCount());

	for (int i = 0; i < QApplication::desktop()->screenCount(); i++)
	{
		RegApp.beginGroup(QString("Screen %1").arg(i));
		RegApp.setValue("size", QApplication::desktop()->availableGeometry(i));
		RegApp.endGroup();
	}
}

void AacsUpdaterWindow::closeEvent(QCloseEvent *ev)
{
	appUiSaveLayout();

	if (m_debugLog) delete(m_debugLog);

	m_debugLog = 0;

	ev->accept();
}

void AacsUpdaterWindow::createActions()
{

}

void AacsUpdaterWindow::createConnections()
{
	connect(m_aacsDatabase, SIGNAL(checkApiEnded(bool)), SLOT(handleCheckApiEnded(bool)));
	connect(m_aacsDatabase, SIGNAL(checkApiTimedOut()), SLOT(handleCheckApiTimedOut()));

	connect(m_aacsDatabase, SIGNAL(loadCentralKeydbEnded(bool)), SLOT(handleLoadCentralKeydbEnded(bool)));
	connect(m_aacsDatabase, SIGNAL(loadCentralKeydbTimedOut()), SLOT(handleLoadCentralKeydbTimedOut()));

	connect(m_aacsDatabase, SIGNAL(newKeysDownloaded(int,int,int)), SLOT(handleFoundNewKeysDownloaded(int,int,int)));
	connect(m_aacsDatabase, SIGNAL(newKeysToUpload(int,int,int)), SLOT(handleFoundNewKeysToUpload(int,int,int)));
	connect(m_aacsDatabase, SIGNAL(installedNewKeysEnded(int,bool)), SLOT(handleInstalledNewKeys(int,bool)));

	connect(m_aacsDatabase, SIGNAL(uploadNewKeysEnded(int,bool)), SLOT(handleUploadNewKeysEnded(int,bool)));
	connect(m_aacsDatabase, SIGNAL(uploadNewKeysTimedOut()), SLOT(handleUploadNewKeysTimedOut()));
}

void AacsUpdaterWindow::createLayout()
{
	m_mainVLayout = new QVBoxLayout(m_mainWidget);
}

void AacsUpdaterWindow::createMenu()
{

}

void AacsUpdaterWindow::createWidgets()
{
	m_mainWidget = new QWidget();

	setCentralWidget(m_mainWidget);
}

void AacsUpdaterWindow::handleCheckApiEnded(bool success)
{
	if (success)
	{
		if (m_aacsDatabase->upgradeRequired())
		{
			m_processingWidget->setImage(":/error.png", QSize(24,24));
			m_processingWidget->setText(tr("Updater program upgrade is required!"), "font-size: 14px; color: #ff420c");

			m_mainVLayout->insertWidget(m_mainVLayout->count() - 2, 
				upgradeBox(tr("Program upgrade required"),
					tr("<b>Please download and install %1 upgrade.</b>").arg(APPNAME), this));
			
			appendCloseButton();

			return;
		}
		
		if (m_aacsDatabase->upgradeSuggested())
		{
			m_mainVLayout->insertWidget(m_mainVLayout->count() - 2, 
				upgradeBox(tr("Program upgrade available"),
					tr("Install %1 upgrade is recommended.").arg(APPNAME), this));

			m_mainVLayout->insertSpacing(m_mainVLayout->count() - 2, 12);
		}

		m_processingWidget->setText(tr("Loading AACS keys from central database..."));

		m_aacsDatabase->startLoadCentralKeydb(LOADCENTRALDB_TIMEOUT);
	}
	else
	{
		m_processingWidget->setImage(":/error.png", QSize(24,24));
		m_processingWidget->setText(tr("Central database connection failed!"), "font-size: 14px; color: #ff420c");
	}
}

void AacsUpdaterWindow::handleCheckApiTimedOut()
{
	m_processingWidget->setImage(":/error.png", QSize(24,24));
	m_processingWidget->setText(tr("Central database not reachable!"), "font-size: 14px; color: #ff420c");
}

void AacsUpdaterWindow::handleFoundNewKeysDownloaded(int hc, int pk, int vuk)
{	
	if (hc)
		appendDone(tr("Downloaded %1 new host certificates").arg(hc));

	if (pk)
		appendDone(tr("Downloaded %1 new processing keys").arg(pk));

	if (vuk)
		appendDone(tr("Downloaded <a href=#vuk>%1 new disc keys</a>").arg(vuk));
}

void AacsUpdaterWindow::handleFoundNewKeysToUpload(int hc, int pk, int vuk)
{	
	if (hc)
		appendDone(tr("Found %1 host certificates to upload").arg(hc));

	if (pk)
		appendDone(tr("Found %1 processing keys to upload").arg(pk));

	if (vuk)
		appendDone(tr("Found <a href=#upvuk>%1 disc keys to upload</a>").arg(vuk));
}

void AacsUpdaterWindow::handleInstalledNewKeys(int n, bool success)
{
	m_aacsKeysInstalled = success;
	m_aacsKeysToInstall = n;

	if (n)
	{
		if (success)
			appendDone(tr("New downloaded keys installed"));
		else
			appendFailed(tr("Failed to install new downloaded keys"));
	}
	else
	{
		appendDone(tr("No new keys to install"));
	}

	m_processingWidget->setText(tr("Uploading new AACS keys..."));

	m_aacsDatabase->startUploadNewKeys();
}

void AacsUpdaterWindow::handleLinkFromAppendDone(QString lnk)
{
	if (lnk == "#upvuk")
	{
		NewKeysDialog dlg(tr("New disc keys to upload"), m_aacsDatabase->newLocalKeys(), this);

		dlg.exec();
	}
	else
	{
		NewKeysDialog dlg(tr("New disc keys downloaded"), m_aacsDatabase->newDiscKeys(), this);

		dlg.exec();
	}
}

void AacsUpdaterWindow::handleLoadCentralKeydbEnded(bool success)
{
	if (success)
	{
		m_processingWidget->setText(tr("Computing new AACS keys..."));

		m_aacsDatabase->startParseLocalConfig();

		qApp->processEvents();  // GUI responsiveness
	}
	else
	{
		m_processingWidget->setImage(":/error.png", QSize(24,24));
		m_processingWidget->setText(tr("AACS keys not downloaded!"), "font-size: 14px; color: #ff420c");
	}
}

void AacsUpdaterWindow::handleLoadCentralKeydbTimedOut()
{
	m_processingWidget->setImage(":/error.png", QSize(24,24));
	m_processingWidget->setText(tr("AACS keys not downloaded!"), "font-size: 14px; color: #ff420c");
}

void AacsUpdaterWindow::handleUploadNewKeysEnded(int n, bool success)
{
	if (n)
	{
		if (success)
			appendDone(tr("Local keys uploaded"));
		else
			appendFailed(tr("Failed to upload keys "));
	}
	else
	{
		appendDone(tr("No keys to upload"));
	}

	if (m_aacsKeysInstalled)
	{
		m_processingWidget->setImage(":/done.png", QSize(26,26));

		if (m_aacsKeysToInstall)
			m_processingWidget->setText(tr("AACS keys updated"), "font-size: 14px; color: #000000");
		else
			m_processingWidget->setText(tr("AACS keys already up-to-date"), "font-size: 14px; color: #000000");
	}
	else
	{
		m_processingWidget->setImage(":/error.png", QSize(26,26));
		m_processingWidget->setText(tr("Failed to update AACS keys!"), "font-size: 14px; color: #ff420c");
	}

	appendCloseButton();
}

void AacsUpdaterWindow::handleUploadNewKeysTimedOut()
{
	appendFailed(tr("New AACS keys not uploaded"));

	if (m_aacsKeysInstalled)
	{
		m_processingWidget->setImage(":/done.png", QSize(26,26));

		if (m_aacsKeysToInstall)
			m_processingWidget->setText(tr("AACS keys updated"), "font-size: 14px; color: #000000");
		else
			m_processingWidget->setText(tr("AACS keys already up-to-date"), "font-size: 14px; color: #000000");
	}
	else
	{
		m_processingWidget->setImage(":/error.png", QSize(26,26));
		m_processingWidget->setText(tr("Failed to update AACS keys!"), "font-size: 14px; color: #ff420c");
	}

	appendCloseButton();
}

void AacsUpdaterWindow::hideEvent(QHideEvent *ev)
{
	if (m_debugLog) m_debugLog->hide();

	ev->accept();
}

void AacsUpdaterWindow::updateAacsDatabase()
{
	m_processingWidget->setText(tr("Checking database API..."));

	m_aacsDatabase->startCheckApi(CHECKAPI_TIMEOUT);
}

QGroupBox *AacsUpdaterWindow::upgradeBox(const QString &title, const QString &text, QWidget *parent)
{
	QGroupBox *box = new QGroupBox(title, parent);

	QLabel *binUrl = new QLabel(tr("<a href='%1'>%2</a>").arg(QString::fromUtf8(m_aacsDatabase->upgradeDownloadBinUrl())).arg(QString::fromUtf8(m_aacsDatabase->upgradeDownloadBin())));
	binUrl->setOpenExternalLinks(true);
	binUrl->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);

	QGridLayout *grid = new QGridLayout(box);
	grid->addWidget(new QLabel(text),
			0, 0, 1, 3);  // no need to add widget in 3rd column
	grid->addWidget(new QLabel(tr("Download installer :")),
			1, 0);
	grid->addWidget(binUrl,
			1, 1, Qt::AlignLeft | Qt::AlignVCenter);
	grid->setColumnStretch(2, 2);

#if !defined(WIN32)
	QLabel *srcUrl = new QLabel(tr("<a href='%1'>%2</a>").arg(QByteArray(m_aacsDatabase->upgradeDownloadSrcUrl())).arg((m_aacsDatabase->upgradeDownloadSrc())));
	srcUrl->setOpenExternalLinks(true);
	srcUrl->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);

	grid->addWidget(new QLabel(tr("Download source code :")), 
			2, 0);
	grid->addWidget(srcUrl,									
			2, 1, Qt::AlignLeft | Qt::AlignVCenter);
#endif
	
	return box;
}
